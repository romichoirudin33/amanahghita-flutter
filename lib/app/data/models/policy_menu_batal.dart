import 'dart:convert';

PolicyMenuBatal policyMenuBatalFromJson(String str) =>
    PolicyMenuBatal.fromJson(json.decode(str));

String policyMenuBatalToJson(PolicyMenuBatal data) =>
    json.encode(data.toJson());

class PolicyMenuBatal {
  PolicyMenuBatal({
    this.success,
    this.status,
    this.data,
  });

  bool? success;
  String? status;
  List<Policy>? data;

  factory PolicyMenuBatal.fromJson(Map<String, dynamic> json) =>
      PolicyMenuBatal(
        success: json["success"],
        status: json["status"],
        data: List<Policy>.from(json["data"].map((x) => Policy.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "success": success,
        "status": status,
        "data": data != null
            ? List<dynamic>.from(data!.map((x) => x.toJson()))
            : null,
      };
}

class Policy {
  Policy({
    this.spaNoGroup,
    this.policyNo,
    this.qq,
    this.namaPerusahaan,
    this.jumlahPeserta,
  });

  String? spaNoGroup;
  String? policyNo;
  String? qq;
  String? namaPerusahaan;
  int? jumlahPeserta;

  factory Policy.fromJson(Map<String, dynamic> json) => Policy(
        spaNoGroup: json["spa_no_group"],
        policyNo: json["policy_no"],
        qq: json["qq"],
        namaPerusahaan: json["nama_perusahaan"],
        jumlahPeserta: json["jumlah_peserta"],
      );

  Map<String, dynamic> toJson() => {
        "spa_no_group": spaNoGroup,
        "policy_no": policyNo,
        "qq": qq,
        "nama_perusahaan": namaPerusahaan,
        "jumlah_peserta": jumlahPeserta,
      };
}

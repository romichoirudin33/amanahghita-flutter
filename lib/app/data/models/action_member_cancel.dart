// To parse this JSON data, do
//
//     final actionMember = actionMemberFromJson(jsonString);

import 'dart:convert';

ActionMember actionMemberFromJson(String str) =>
    ActionMember.fromJson(json.decode(str));

String actionMemberToJson(ActionMember data) => json.encode(data.toJson());

class ActionMember {
  ActionMember({
    this.success,
    this.status,
    this.data,
  });

  bool? success;
  String? status;
  List<Data>? data;

  factory ActionMember.fromJson(Map<String, dynamic> json) => ActionMember(
        success: json["success"],
        status: json["status"],
        data: json["data"] != null
            ? List<Data>.from(json["data"].map((x) => Data.fromJson(x)))
            : null,
      );

  Map<String, dynamic> toJson() => {
        "success": success,
        "status": status,
        "data": data,
      };
}

class Data {
  Data({
    this.policyNo,
    this.noPeserta,
    this.status,
    this.statusPolis,
    this.keterangan,
  });

  String? policyNo;
  String? noPeserta;
  bool? status;
  dynamic statusPolis;
  String? keterangan;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        policyNo: json["policy_no"],
        noPeserta: json["no_peserta"],
        status: json["status"],
        statusPolis: json["status_polis"],
        keterangan: json["keterangan"],
      );

  Map<String, dynamic> toJson() => {
        "policy_no": policyNo,
        "no_peserta": noPeserta,
        "status": status,
        "status_polis": statusPolis,
        "keterangan": keterangan
      };
}

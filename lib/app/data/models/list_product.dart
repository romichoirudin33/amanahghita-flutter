import 'dart:convert';

ListProduct listProductFromJson(String str) =>
    ListProduct.fromJson(json.decode(str));

String listProductToJson(ListProduct data) => json.encode(data.toJson());

class ListProduct {
  ListProduct({
    this.success,
    this.status,
    this.data,
  });

  bool? success;
  String? status;
  List<Product>? data;

  factory ListProduct.fromJson(Map<String, dynamic> json) => ListProduct(
        success: json["success"],
        status: json["status"],
        data: List<Product>.from(json["data"].map((x) => Product.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "success": success,
        "status": status,
        "data": data != null
            ? List<dynamic>.from(data!.map((x) => x.toJson()))
            : null,
      };
}

class Product {
  Product({
    this.spaNoGroup,
    this.policyNo,
    this.policyHolder,
    this.productId,
    this.productCode,
    this.productName,
    this.benefitId,
    this.description,
    this.benefitsFlag,
  });

  String? spaNoGroup;
  String? policyNo;
  String? policyHolder;
  String? productId;
  String? productCode;
  String? productName;
  String? benefitId;
  String? description;
  String? benefitsFlag;

  factory Product.fromJson(Map<String, dynamic> json) => Product(
        spaNoGroup: json["spa_no_group"],
        policyNo: json["policy_no"],
        policyHolder: json["policy_holder"],
        productId: json["product_id"],
        productCode: json["product_code"],
        productName: json["product_name"],
        benefitId: json["benefit_id"],
        description: json["description"],
        benefitsFlag: json["benefits_flag"],
      );

  Map<String, dynamic> toJson() => {
        "spa_no_group": spaNoGroup,
        "policy_no": policyNo,
        "policy_holder": policyHolder,
        "product_id": productId,
        "product_code": productCode,
        "product_name": productName,
        "benefit_id": benefitId,
        "description": description,
        "benefits_flag": benefitsFlag,
      };
}

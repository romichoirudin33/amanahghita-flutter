// ignore_for_file: file_names

import 'dart:convert';

List<Policy> policyFromJson(String str) =>
    List<Policy>.from(json.decode(str).map((x) => Policy.fromJson(x)));

String policyToJson(List<Policy> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Policy {
  Policy({
    this.policyNo,
    this.policyHolder,
    this.spaNoGroup,
    this.productId,
    this.productName,
  });

  String? policyNo;
  String? policyHolder;
  String? spaNoGroup;
  String? productId;
  String? productName;

  factory Policy.fromJson(Map<String, dynamic> json) => Policy(
        policyNo: json["policy_no"] ?? '',
        policyHolder: json["policy_holder"] ?? '',
        spaNoGroup: json["spa_no_group"] ?? '',
        productId: json["product_id"] ?? '',
        productName: json["product_name"] ?? '',
      );

  Map<String, dynamic> toJson() => {
        "policy_no": policyNo,
        "policy_holder": policyHolder,
        "spa_no_group": spaNoGroup,
        "product_id": productId,
        "product_name": productName,
      };
}

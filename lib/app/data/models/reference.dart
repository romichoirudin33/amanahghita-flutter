import 'dart:convert';

Reference referenceFromJson(String str) => Reference.fromJson(json.decode(str));

String referenceToJson(Reference data) => json.encode(data.toJson());

class Reference {
  Reference({
    this.success,
    this.status,
    this.data,
  });

  bool? success;
  String? status;
  List<DetailRefrence>? data;

  factory Reference.fromJson(Map<String, dynamic> json) => Reference(
        success: json["success"],
        status: json["status"],
        data: List<DetailRefrence>.from(
            json["data"].map((x) => DetailRefrence.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "success": success,
        "status": status,
        "data": data != null
            ? List<dynamic>.from(data!.map((x) => x.toJson()))
            : null,
      };
}

class DetailRefrence {
  DetailRefrence({
    this.id,
    this.kode,
    this.keterangan,
  });

  String? id;
  String? kode;
  String? keterangan;

  factory DetailRefrence.fromJson(Map<String, dynamic> json) => DetailRefrence(
        id: json["id"],
        kode: json["kode"],
        keterangan: json["keterangan"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "kode": kode,
        "keterangan": keterangan,
      };
}

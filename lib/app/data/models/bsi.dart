import 'dart:convert';

Bsi bsiFromJson(String str) => Bsi.fromJson(json.decode(str));

String bsiToJson(Bsi data) => json.encode(data.toJson());

class Bsi {
  Bsi({
    this.success,
    this.status,
    this.data,
  });

  bool? success;
  String? status;
  List<Cabang>? data;

  factory Bsi.fromJson(Map<String, dynamic> json) => Bsi(
        success: json["success"],
        status: json["status"],
        data: List<Cabang>.from(json["data"].map((x) => Cabang.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "success": success,
        "status": status,
        "data": data != null
            ? List<dynamic>.from(data!.map((x) => x.toJson()))
            : null,
      };
}

class Cabang {
  Cabang({
    this.kodeCabang,
    this.namaCabang,
  });

  String? kodeCabang;
  String? namaCabang;

  factory Cabang.fromJson(Map<String, dynamic> json) => Cabang(
        kodeCabang: json["kode_cabang"],
        namaCabang: json["nama_cabang"],
      );

  Map<String, dynamic> toJson() => {
        "kode_cabang": kodeCabang,
        "nama_cabang": namaCabang,
      };
}

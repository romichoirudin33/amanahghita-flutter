// To parse this JSON data, do
//
//     final listPolicy = listPolicyFromJson(jsonString);

import 'dart:convert';

ListPolicy listPolicyFromJson(String str) =>
    ListPolicy.fromJson(json.decode(str));

String listPolicyToJson(ListPolicy data) => json.encode(data.toJson());

class ListPolicy {
  ListPolicy({
    this.success,
    this.status,
    this.data,
  });

  bool? success;
  String? status;
  List<Policy>? data;

  factory ListPolicy.fromJson(Map<String, dynamic> json) => ListPolicy(
        success: json["success"],
        status: json["status"],
        data: List<Policy>.from(json["data"].map((x) => Policy.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "success": success,
        "status": status,
        "data": data != null
            ? List<dynamic>.from(data!.map((x) => x.toJson()))
            : null,
      };
}

class Policy {
  Policy({
    this.policyNo,
    this.policyHolder,
    this.spaNoGroup,
    this.productId,
    this.productName,
  });

  String? policyNo;
  String? policyHolder;
  String? spaNoGroup;
  String? productId;
  String? productName;

  factory Policy.fromJson(Map<String, dynamic> json) => Policy(
        policyNo: json["policy_no"],
        policyHolder: json["policy_holder"],
        spaNoGroup: json["spa_no_group"],
        productId: json["product_id"],
        productName: json["product_name"],
      );

  Map<String, dynamic> toJson() => {
        "policy_no": policyNo,
        "policy_holder": policyHolder,
        "spa_no_group": spaNoGroup,
        "product_id": productId,
        "product_name": productName,
      };
}

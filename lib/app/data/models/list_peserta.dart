// To parse this JSON data, do
//
//     final listPeserta = listPesertaFromJson(jsonString);

import 'dart:convert';

ListPeserta listPesertaFromJson(String str) =>
    ListPeserta.fromJson(json.decode(str));

String listPesertaToJson(ListPeserta data) => json.encode(data.toJson());

class ListPeserta {
  ListPeserta({
    this.success,
    this.status,
    this.data,
  });

  bool? success;
  String? status;
  List<Peserta>? data;

  factory ListPeserta.fromJson(Map<String, dynamic> json) => ListPeserta(
        success: json["success"],
        status: json["status"],
        data: List<Peserta>.from(json["data"].map((x) => Peserta.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "success": success,
        "status": status,
        "data": data != null
            ? List<dynamic>.from(data!.map((x) => x.toJson()))
            : null,
      };
}

class Peserta {
  Peserta({
    this.policyNo,
    this.noPeserta,
    this.nama,
    this.tanggalLahir,
    this.fromDate,
    this.thruDate,
    this.status,
    this.usia,
    this.totalPremi,
    this.totalBenefit,
    this.extraRate,
    this.rate,
    this.productId,
    this.weight,
    this.height,
    this.acceptationDate,
    this.noInvoice,
    this.noReferensi,
    this.noBatch,
    this.stncDate,
    this.underwritingStatus,
  });

  String? policyNo;
  String? noPeserta;
  String? nama;
  DateTime? tanggalLahir;
  DateTime? fromDate;
  DateTime? thruDate;
  String? status;
  int? usia;
  String? totalPremi;
  String? totalBenefit;
  String? extraRate;
  String? rate;
  String? productId;
  String? weight;
  String? height;
  DateTime? acceptationDate;
  dynamic noInvoice;
  dynamic noReferensi;
  dynamic noBatch;
  dynamic stncDate;
  String? underwritingStatus;

  factory Peserta.fromJson(Map<String, dynamic> json) => Peserta(
        policyNo: json["policy_no"],
        noPeserta: json["no_peserta"],
        nama: json["nama"],
        tanggalLahir: DateTime.parse(json["tanggal_lahir"]),
        fromDate: DateTime.parse(json["from_date"]),
        thruDate: DateTime.parse(json["thru_date"]),
        status: json["status"],
        usia: json["usia"],
        totalPremi: json["total_premi"],
        totalBenefit: json["total_benefit"],
        extraRate: json["extra_rate"],
        rate: json["rate"],
        productId: json["product_id"],
        weight: json["weight"],
        height: json["height"],
        acceptationDate: json["acceptation_date"] == null
            ? json["acceptation_date"]
            : DateTime.parse(json["acceptation_date"]),
        noInvoice: json["no_invoice"],
        noReferensi: json["no_referensi"],
        noBatch: json["no_batch"],
        stncDate: json["stnc_date"],
        underwritingStatus: json["underwriting_status"],
      );

  Map<String, dynamic> toJson() => {
        "policy_no": policyNo,
        "no_peserta": noPeserta,
        "nama": nama,
        "tanggal_lahir": tanggalLahir != null
            ? "${tanggalLahir!.year.toString().padLeft(4, '0')}-${tanggalLahir!.month.toString().padLeft(2, '0')}-${tanggalLahir!.day.toString().padLeft(2, '0')}"
            : null,
        "from_date": fromDate != null
            ? "${fromDate!.year.toString().padLeft(4, '0')}-${fromDate!.month.toString().padLeft(2, '0')}-${fromDate!.day.toString().padLeft(2, '0')}"
            : null,
        "thru_date": thruDate != null
            ? "${thruDate!.year.toString().padLeft(4, '0')}-${thruDate!.month.toString().padLeft(2, '0')}-${thruDate!.day.toString().padLeft(2, '0')}"
            : null,
        "status": status,
        "usia": usia,
        "total_premi": totalPremi,
        "total_benefit": totalBenefit,
        "extra_rate": extraRate,
        "rate": rate,
        "product_id": productId,
        "weight": weight,
        "height": height,
        "acceptation_date": acceptationDate != null
            ? "${acceptationDate!.year.toString().padLeft(4, '0')}-${acceptationDate!.month.toString().padLeft(2, '0')}-${acceptationDate!.day.toString().padLeft(2, '0')}"
            : null,
        "no_invoice": noInvoice,
        "no_referensi": noReferensi,
        "no_batch": noBatch,
        "stnc_date": stncDate,
        "underwriting_status": underwritingStatus,
      };
}

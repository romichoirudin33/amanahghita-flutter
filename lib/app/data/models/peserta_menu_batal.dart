// ignore_for_file: file_names

import 'dart:convert';

PesertaMenuBatal pesertaMenuBatalFromJson(String str) =>
    PesertaMenuBatal.fromJson(json.decode(str));

String pesertaMenuBatalToJson(PesertaMenuBatal data) =>
    json.encode(data.toJson());

class PesertaMenuBatal {
  PesertaMenuBatal({
    this.success,
    this.status,
    this.data,
    this.page,
  });

  bool? success;
  String? status;
  List<Peserta>? data;
  int? page;

  factory PesertaMenuBatal.fromJson(Map<String, dynamic> json) =>
      PesertaMenuBatal(
        success: json["success"],
        status: json["status"],
        data: List<Peserta>.from(json["data"].map((x) => Peserta.fromJson(x))),
        page: json["page"],
      );

  Map<String, dynamic> toJson() => {
        "success": success,
        "status": status,
        "data": data != null
            ? List<dynamic>.from(data!.map((x) => x.toJson()))
            : null,
        "page": page,
      };
}

class Peserta {
  Peserta({
    this.id,
    this.policyNo,
    this.noPeserta,
    this.nama,
    this.namaPerusahaan,
    this.tanggalLahir,
    this.fromDate,
    this.thruDate,
    this.status,
    this.usia,
    this.totalPremi,
    this.totalBenefit,
    this.noInvoice,
    this.createdStamp,
  });

  String? id;
  String? policyNo;
  String? noPeserta;
  String? nama;
  String? namaPerusahaan;
  DateTime? tanggalLahir;
  DateTime? fromDate;
  DateTime? thruDate;
  String? status;
  int? usia;
  int? totalPremi;
  int? totalBenefit;
  dynamic noInvoice;
  DateTime? createdStamp;

  factory Peserta.fromJson(Map<String, dynamic> json) => Peserta(
        id: json["id"],
        policyNo: json["policy_no"],
        noPeserta: json["no_peserta"],
        nama: json["nama"],
        namaPerusahaan: json["nama_perusahaan"],
        tanggalLahir: DateTime.parse(json["tanggal_lahir"]),
        fromDate: DateTime.parse(json["from_date"]),
        thruDate: DateTime.parse(json["thru_date"]),
        status: json["status"],
        usia: json["usia"],
        totalPremi: json["total_premi"],
        totalBenefit: json["total_benefit"],
        noInvoice: json["no_invoice"],
        createdStamp: DateTime.parse(json["created_stamp"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "policy_no": policyNo,
        "no_peserta": noPeserta,
        "nama": nama,
        "nama_perusahaan": namaPerusahaan,
        "tanggal_lahir": tanggalLahir != null
            ? "${tanggalLahir?.year.toString().padLeft(4, '0')}-${tanggalLahir?.month.toString().padLeft(2, '0')}-${tanggalLahir?.day.toString().padLeft(2, '0')}"
            : null,
        "from_date": fromDate?.toIso8601String(),
        "thru_date": thruDate?.toIso8601String(),
        "status": status,
        "usia": usia,
        "total_premi": totalPremi,
        "total_benefit": totalBenefit,
        "no_invoice": noInvoice,
        "created_stamp": tanggalLahir != null
            ? "${createdStamp?.year.toString().padLeft(4, '0')}-${createdStamp?.month.toString().padLeft(2, '0')}-${createdStamp?.day.toString().padLeft(2, '0')}"
            : null,
      };
}

import 'dart:convert';

List<FilterPencarian> filterPencarianFromJson(String str) =>
    List<FilterPencarian>.from(
        json.decode(str).map((x) => FilterPencarian.fromJson(x)));

String filterPencarianToJson(List<FilterPencarian> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class FilterPencarian {
  FilterPencarian({
    this.label,
    this.value,
  });

  String? label;
  String? value;

  factory FilterPencarian.fromJson(Map<String, dynamic> json) =>
      FilterPencarian(
        label: json["label"],
        value: json["value"],
      );

  Map<String, dynamic> toJson() => {
        "label": label,
        "value": value,
      };
}

// ignore_for_file: file_names

import 'dart:convert';

List<Peserta> pesertaFromJson(String str) =>
    List<Peserta>.from(json.decode(str).map((x) => Peserta.fromJson(x)));

String pesertaToJson(List<Peserta> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Peserta {
  Peserta({
    this.policyNo,
    this.noPeserta,
    this.nama,
    this.tanggalLahir,
    this.fromDate,
    this.thruDate,
    this.status,
    this.usia,
    this.totalPremi,
    this.totalBenefit,
    this.extraRate,
    this.rate,
    this.productId,
    this.weight,
    this.height,
    this.acceptationDate,
    this.noInvoice,
    this.noReferensi,
    this.noBatch,
    this.stncDate,
  });

  String? policyNo;
  String? noPeserta;
  String? nama;
  String? tanggalLahir;
  String? fromDate;
  String? thruDate;
  String? status;
  int? usia;
  String? totalPremi;
  String? totalBenefit;
  String? extraRate;
  String? rate;
  String? productId;
  String? weight;
  String? height;
  String? acceptationDate;
  dynamic noInvoice;
  dynamic noReferensi;
  dynamic noBatch;
  dynamic stncDate;

  factory Peserta.fromJson(Map<String, dynamic> json) => Peserta(
        policyNo: json["policy_no"],
        noPeserta: json["no_peserta"],
        nama: json["nama"],
        tanggalLahir: json["tanggal_lahir"],
        fromDate: json["from_date"],
        thruDate: json["thru_date"],
        status: json["status"],
        usia: json["usia"],
        totalPremi: json["total_premi"],
        totalBenefit: json["total_benefit"],
        extraRate: json["extra_rate"],
        rate: json["rate"],
        productId: json["product_id"],
        weight: json["weight"],
        height: json["height"],
        acceptationDate: json["acceptation_date"],
        noInvoice: json["no_invoice"],
        noReferensi: json["no_referensi"],
        noBatch: json["no_batch"],
        stncDate: json["stnc_date"],
      );

  Map<String, dynamic> toJson() => {
        "policy_no": policyNo,
        "no_peserta": noPeserta,
        "nama": nama,
        "tanggal_lahir": tanggalLahir,
        "from_date": fromDate,
        "thru_date": thruDate,
        "status": status,
        "usia": usia,
        "total_premi": totalPremi,
        "total_benefit": totalBenefit,
        "extra_rate": extraRate,
        "rate": rate,
        "product_id": productId,
        "weight": weight,
        "height": height,
        "acceptation_date": acceptationDate,
        "no_invoice": noInvoice,
        "no_referensi": noReferensi,
        "no_batch": noBatch,
        "stnc_date": stncDate,
      };
}

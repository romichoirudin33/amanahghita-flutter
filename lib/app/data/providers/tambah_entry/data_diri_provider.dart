import 'dart:convert';

import 'package:amanahgitha_flutter/app/data/api_client.dart';
import 'package:amanahgitha_flutter/app/data/models/reference.dart';
import 'package:flutter/foundation.dart';
import 'package:get/get.dart';

class DataDiriProvider extends GetConnect {
  Future<List<DetailRefrence>?> getReference(Map data) async {
    final response = await post(
      ApiClient.address + '?data=get_reference',
      data,
      headers: ApiClient.headers,
      contentType: 'application/x-www-form-urlencoded',
    );
    if (response.statusCode == 200) {
      var res = Reference.fromJson(json.decode(response.body));
      if (kDebugMode) {
        print(res.data.toString());
      }
      return res.data;
    } else {
      return null;
    }
  }

  Future<String?> getBmi(Map data) async {
    final response = await post(
      ApiClient.address + '?data=get_bmi',
      data,
      headers: ApiClient.headers,
      contentType: 'application/x-www-form-urlencoded',
    );
    return response.bodyString;
    // Map<String, dynamic> res = json.decode(response.body);
    // if (response.statusCode == 200 && res['success']) {
    //   return res['data']['end_of_contract'].toString();
    // } else {
    //   return null;
    // }
  }

  Future<bool?> submitMember(Map data) async {
    final response = await post(
      ApiClient.address + '?data=submit_member',
      data,
      headers: ApiClient.headers,
      contentType: 'application/x-www-form-urlencoded',
    );

    if (response.statusCode == 200) {
      Map<String, dynamic> res = json.decode(response.body);
      if (kDebugMode) {
        print("submit member : " + res.toString());
      }
      return res['success'];
    } else {
      return null;
    }
  }
}

// ignore_for_file: avoid_print

import 'dart:convert';

import 'package:amanahgitha_flutter/app/data/api_client.dart';
import 'package:amanahgitha_flutter/app/data/models/bsi.dart';
import 'package:amanahgitha_flutter/app/data/models/list_product.dart';
import 'package:flutter/foundation.dart';
import 'package:get/get.dart';

class ProductProvider extends GetConnect {
  Future<List<Cabang>?> getListCabang(Map data) async {
    final response = await post(
      ApiClient.address + '?data=get_list_bsi',
      data,
      headers: ApiClient.headers,
      contentType: 'application/x-www-form-urlencoded',
    );
    if (response.statusCode == 200) {
      var res = Bsi.fromJson(json.decode(response.body));
      if (kDebugMode) {
        print(res.data?.length);
      }
      return res.data;
    } else {
      return null;
    }
  }

  Future<List<Product>?> getProductBenefits(Map data) async {
    final response = await post(
      ApiClient.address + '?data=get_product_benefits',
      data,
      headers: ApiClient.headers,
      contentType: 'application/x-www-form-urlencoded',
    );
    if (response.statusCode == 200) {
      var res = ListProduct.fromJson(json.decode(response.body));
      if (kDebugMode) {
        print("getProductBenefit : " + res.data.toString());
      }
      return res.data;
    } else {
      return null;
    }
  }

  Future<Map<String, dynamic>?> getCalculate(Map data) async {
    final response = await post(
      ApiClient.address + '?data=calculate_tabarru',
      data,
      headers: ApiClient.headers,
      contentType: 'application/x-www-form-urlencoded',
    );
    Map<String, dynamic> res = json.decode(response.body);
    if (response.statusCode == 200 && res['success']) {
      if (kDebugMode) {
        print("getCalculate : " + res['data'][0].toString());
      }
      return res['data'][0];
    } else {
      return null;
    }
  }

  Future<String?> getAge(Map data) async {
    final response = await post(
      ApiClient.address + '?data=get_age',
      data,
      headers: ApiClient.headers,
      contentType: 'application/x-www-form-urlencoded',
    );
    Map<String, dynamic> res = json.decode(response.body);
    if (response.statusCode == 200 && res['success']) {
      return res['data'][0]['usia'].toString();
    } else {
      return null;
    }
  }

  Future<String?> getPaymentMethod(Map data) async {
    final response = await post(
      ApiClient.address + '?data=get_payment_method',
      data,
      headers: ApiClient.headers,
      contentType: 'application/x-www-form-urlencoded',
    );
    Map<String, dynamic> res = json.decode(response.body);
    if (response.statusCode == 200 && res['success']) {
      return res['data']['payment_methods'].toString();
    } else {
      return null;
    }
  }

  Future<String?> getEndOfContract(Map data) async {
    final response = await post(
      ApiClient.address + '?data=get_endofcontract',
      data,
      headers: ApiClient.headers,
      contentType: 'application/x-www-form-urlencoded',
    );
    Map<String, dynamic> res = json.decode(response.body);
    if (response.statusCode == 200 && res['success']) {
      return res['data']['end_of_contract'].toString();
    } else {
      return null;
    }
  }
}

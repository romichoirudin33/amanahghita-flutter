// ignore_for_file: avoid_print

import 'dart:async';
import 'dart:convert';

import 'package:amanahgitha_flutter/app/data/api_client.dart';
import 'package:amanahgitha_flutter/app/data/models/action_member.dart';
import 'package:amanahgitha_flutter/app/data/models/filter_pencarian.dart';
import 'package:amanahgitha_flutter/app/data/models/peserta_menu_batal.dart';
import 'package:get/get.dart';

class PesertaProvider extends GetConnect {
  Future<List<FilterPencarian>?> getFilterPeserta(Map data) async {
    final response = await post(
      ApiClient.address +
          '?data=get_field_search_list_member_policy_menu_batal',
      data,
      headers: ApiClient.headers,
      contentType: 'application/x-www-form-urlencoded',
    );
    if (response.statusCode == 200) {
      return filterPencarianFromJson(response.body);
    } else {
      return null;
    }
  }

  Future<List<FilterPencarian>?> getFilter(Map data) async {
    final response = await post(
      ApiClient.address + '?data=get_filter_list_member_policy_menu_batal',
      data,
      headers: ApiClient.headers,
      contentType: 'application/x-www-form-urlencoded',
    );
    if (response.statusCode == 200) {
      return filterPencarianFromJson(response.body);
    } else {
      return null;
    }
  }

  Future<List<FilterPencarian>?> getFilterCancel(Map data) async {
    final response = await post(
      ApiClient.address +
          '?data=get_filter_list_member_policy_menu_batal_delete',
      data,
      headers: ApiClient.headers,
      contentType: 'application/x-www-form-urlencoded',
    );
    if (response.statusCode == 200) {
      return filterPencarianFromJson(response.body);
    } else {
      return null;
    }
  }

  Future<PesertaMenuBatal?> getPeserta(Map data) async {
    final response = await post(
      ApiClient.address + '?data=group_display_member_policy_null',
      data,
      headers: ApiClient.headers,
      contentType: 'application/x-www-form-urlencoded',
    );
    print(response.body.toString());
    if (response.statusCode == 200) {
      return PesertaMenuBatal.fromJson(json.decode(response.bodyString ?? ''));
    } else {
      return null;
    }
  }

  Future<List<Peserta>?> getListPeserta(Map data) async {
    final response = await post(
      ApiClient.address + '?data=group_display_member_policy_null',
      data,
      headers: ApiClient.headers,
      contentType: 'application/x-www-form-urlencoded',
    );
    if (response.statusCode == 200) {
      var res = PesertaMenuBatal.fromJson(json.decode(response.body));
      return res.data;
    } else {
      return null;
    }
  }

  Future<ActionMember?> deleteMember(Map data) async {
    final response = await post(
      ApiClient.address + '?data=action_member_policy_deleted',
      data,
      headers: ApiClient.headers,
      contentType: 'application/x-www-form-urlencoded',
    );
    print(response.body.toString());
    if (response.statusCode == 200) {
      return ActionMember.fromJson(json.decode(response.bodyString ?? ''));
    } else {
      return null;
    }
  }

  Future<String?> cetakSertifikat(Map data) async {
    final response = await post(
      ApiClient.address + '?data=cetak_sertifikat',
      data,
      headers: ApiClient.headers,
      contentType: 'application/x-www-form-urlencoded',
    ).timeout(
      const Duration(seconds: 30),
      onTimeout: () => throw TimeoutException('Can\'t connect in 10 seconds.'),
    );
    print("res ${response.statusText}");
    // Map<String, dynamic> res = json.decode(response.body);
    // if (response.statusCode == 200) {
    //   return res['link'].toString();
    // } else {
    //   return null;
    // }
  }
}

// ignore_for_file: avoid_print

import 'dart:convert';

import 'package:amanahgitha_flutter/app/data/api_client.dart';
import 'package:amanahgitha_flutter/app/data/models/filter_pencarian.dart';
import 'package:amanahgitha_flutter/app/data/models/policy_menu_batal.dart';
import 'package:flutter/foundation.dart';
import 'package:get/get.dart';

class PolicyProvider extends GetConnect {
  Future<List<FilterPencarian>?> getFilterPolicy(Map data) async {
    final response = await post(
      ApiClient.address + '?data=get_field_search_list_policy_menu_batal',
      data,
      headers: ApiClient.headers,
      contentType: 'application/x-www-form-urlencoded',
    );
    if (response.statusCode == 200) {
      return filterPencarianFromJson(response.body);
    } else {
      return null;
    }
  }

  Future<List<Policy>?> getListPolicy(Map data) async {
    final response = await post(
      ApiClient.address + '?data=group_display_policy',
      data,
      headers: ApiClient.headers,
      contentType: 'application/x-www-form-urlencoded',
    );
    if (response.statusCode == 200) {
      var res = PolicyMenuBatal.fromJson(json.decode(response.body));
      if (kDebugMode) {
        print(res.data.toString());
      }
      return res.data;
    } else {
      return null;
    }
  }
}

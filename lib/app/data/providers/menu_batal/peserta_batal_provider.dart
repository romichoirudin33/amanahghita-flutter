// ignore_for_file: avoid_print

import 'dart:convert';

import 'package:amanahgitha_flutter/app/data/api_client.dart';
import 'package:amanahgitha_flutter/app/data/models/action_member_cancel.dart';
import 'package:amanahgitha_flutter/app/data/models/filter_pencarian.dart';
import 'package:amanahgitha_flutter/app/data/models/peserta_menu_batal.dart';
import 'package:flutter/foundation.dart';
import 'package:get/get.dart';

class PesertaBatalProvider extends GetConnect {
  Future<List<FilterPencarian>?> getFilterPeserta(Map data) async {
    final response = await post(
      ApiClient.address +
          '?data=get_field_search_list_member_policy_menu_batal',
      data,
      headers: ApiClient.headers,
      contentType: 'application/x-www-form-urlencoded',
    );
    if (response.statusCode == 200) {
      return filterPencarianFromJson(response.body);
    } else {
      return null;
    }
  }

  Future<List<FilterPencarian>?> getFilter(Map data) async {
    final response = await post(
      ApiClient.address + '?data=get_filter_list_member_policy_menu_batal',
      data,
      headers: ApiClient.headers,
      contentType: 'application/x-www-form-urlencoded',
    );
    if (response.statusCode == 200) {
      return filterPencarianFromJson(response.body);
    } else {
      return null;
    }
  }

  Future<PesertaMenuBatal?> getPeserta(Map data) async {
    final response = await post(
      ApiClient.address + '?data=group_display_member_policy_cancel',
      data,
      headers: ApiClient.headers,
      contentType: 'application/x-www-form-urlencoded',
    );
    if (response.statusCode == 200) {
      return PesertaMenuBatal.fromJson(json.decode(response.bodyString ?? ''));
    } else {
      return null;
    }
  }

  Future<List<Peserta>?> getListPeserta(Map data) async {
    final response = await post(
      ApiClient.address + '?data=group_display_member_policy_cancel',
      data,
      headers: ApiClient.headers,
      contentType: 'application/x-www-form-urlencoded',
    );
    if (response.statusCode == 200) {
      var res = PesertaMenuBatal.fromJson(json.decode(response.body));
      if (kDebugMode) {
        print(res.data.toString());
      }
      return res.data;
    } else {
      return null;
    }
  }

  Future<ActionMember?> deleteMember(Map data) async {
    final response = await post(
      ApiClient.address + '?data=action_member_policy_undeleted',
      data,
      headers: ApiClient.headers,
      contentType: 'application/x-www-form-urlencoded',
    );
    print(response.body.toString());
    if (response.statusCode == 200) {
      return ActionMember.fromJson(json.decode(response.bodyString ?? ''));
    } else {
      return null;
    }
  }
}

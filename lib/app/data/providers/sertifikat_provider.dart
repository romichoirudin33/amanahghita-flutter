import 'dart:async';
import 'dart:convert';

import 'package:amanahgitha_flutter/app/data/api_client.dart';

import 'package:http/http.dart' as http;

class SertifikatProvider {
  Future<Map<String, dynamic>?> cetakSertifikat(Map data) async {
    final response = await http.post(
      Uri.parse(ApiClient.address + '?data=cetak_sertifikat'),
      body: data,
      headers: ApiClient.headers,
    );
    if (response.statusCode == 200) {
      Map<String, dynamic> res = json.decode(response.body);
      return res;
      // if (res['status']) {
      //   return res['link'].toString();
      // } else {
      //   return res['keterangan'].toString();
      // }
    } else {
      return null;
    }
  }
}

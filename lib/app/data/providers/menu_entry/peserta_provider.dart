import 'dart:convert';

import 'package:amanahgitha_flutter/app/data/api_client.dart';
import 'package:amanahgitha_flutter/app/data/models/list_peserta.dart';
import 'package:get/get.dart';

class PesertaProvider extends GetConnect {
  Future<Map<String, dynamic>?> getFilterPeserta(Map data) async {
    final response = await post(
      ApiClient.address + '?data=get_filter_list_peserta',
      data,
      headers: ApiClient.headers,
      contentType: 'application/x-www-form-urlencoded',
    );
    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      return null;
    }
  }

  Future<List<Peserta>?> getListPeserta(Map data) async {
    final response = await post(
      ApiClient.address + '?data=get_list_peserta',
      data,
      headers: ApiClient.headers,
      contentType: 'application/x-www-form-urlencoded',
    );
    if (response.statusCode == 200) {
      var res = ListPeserta.fromJson(json.decode(response.body));
      return res.data;
    } else {
      return null;
    }
  }
}

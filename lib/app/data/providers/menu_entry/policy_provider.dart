// ignore_for_file: avoid_print

import 'dart:convert';

import 'package:amanahgitha_flutter/app/data/api_client.dart';
import 'package:amanahgitha_flutter/app/data/models/list_policy.dart';
import 'package:flutter/foundation.dart';
import 'package:get/get.dart';

class PolicyProvider extends GetConnect {
  Future<Map<String, dynamic>?> getFilterPolicy(Map data) async {
    final response = await post(
      ApiClient.address + '?data=get_filter_list_policy',
      data,
      headers: ApiClient.headers,
      contentType: 'application/x-www-form-urlencoded',
    );
    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      return null;
    }
  }

  Future<List<Policy>?> getListPolicy(Map data) async {
    final response = await post(
      ApiClient.address + '?data=get_list_policy',
      data,
      headers: ApiClient.headers,
      contentType: 'application/x-www-form-urlencoded',
    );
    if (response.statusCode == 200) {
      var res = ListPolicy.fromJson(json.decode(response.body));
      if (kDebugMode) {
        print(res.data.toString());
      }
      return res.data;
    } else {
      return null;
    }
  }
}

import 'dart:convert';

import 'package:amanahgitha_flutter/app/data/api_client.dart';
import 'package:get/get.dart';

class AuthProvider extends GetConnect {
  Future<Map<String, dynamic>?> login(Map data) async {
    final response = await post(
      ApiClient.address + '?data=login',
      data,
      headers: ApiClient.headers,
      contentType: 'application/x-www-form-urlencoded',
    );
    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      return null;
    }
  }
}

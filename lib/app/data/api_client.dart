import 'package:flutter_dotenv/flutter_dotenv.dart';

class ApiClient {
  static String address =
      dotenv.env['SERVER_ADDRESS'] ?? 'http://sasambo.amanahgitha.com/';

  static Map<String, String> headers = {
    'id': '67f656a7e55f4b8abf745d8519d038f6',
  };
}

import 'package:flutter/cupertino.dart';

class EmptyWidget extends StatelessWidget {
  const EmptyWidget({Key? key, required this.msg, this.height})
      : super(key: key);

  final String msg;
  final double? height;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: height ?? 100,
      child: Center(
        child: Text(
          msg,
          textAlign: TextAlign.center,
        ),
      ),
    );
  }
}

import 'package:amanahgitha_flutter/app/utils/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

snackbar({required String title, required String message, Color? color}) {
  return Get.snackbar(
    title,
    message,
    backgroundColor: color ?? successColor,
    snackPosition: SnackPosition.BOTTOM,
    colorText: whiteColor,
    borderRadius: 5,
  );
}

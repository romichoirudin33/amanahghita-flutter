// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';

Color mainColor = Color(0xFF0089E2);
Color backgroundColor = Color(0xFFF1F1F1);
Color whiteColor = Color(0xFFFFFFFF);

// text
Color primaryText = gray900;
Color secondaryText = gray500;
Color placeholderText = gray400;

// ui
Color successColor = green400;
Color successBorderColor = green700;
Color dangerColor = Color(0xFFDA5552);
Color dangerBorderColor = Color(0xFFCC444B);
Color warningColor = Color(0xFFF3752B);
Color warningBorderColor = Color(0xFFE45C3A);
Color infoColor = Color(0xFF00B4D8);
Color infoBorderColor = Color(0xFF0096C7);

Color disableColor = Color(0xFFF1F1F1);

Color linkColor = Color(0xFF626EEF);
Color logoColor = Color(0xFF0089E2);
Color deleteColor = Color(0xFFFF3B30);

// themes
Color indigo700 = Color(0xFF384EC7);
Color indigo400 = Color(0xFF4B61DD);
Color indigo50 = Color(0xFFE4F3F9);
Color avocado = Color(0xFF58BA47);

// gray
Color gray900 = Color(0xFF1F1F1F);
Color gray = Color(0xFF474747);
Color gray500 = Color(0xFF717171);
Color gray400 = Color(0xFF919191);
Color gray100 = Color(0xFFE1E1E1);
Color gray50 = Color(0xFFF1F1F1);
Color gray25 = Color(0xFFF8F8F8);

//green
Color green700 = Color(0xFF40916C);
Color green400 = Color(0xFF52B788);
Color green200 = Color(0xFFB7E4C7);
Color green50 = Color(0xFFD8F3DC);

//red
Color red700 = Color(0xFFCC444B);
Color red400 = Color(0xFFDA5552);
Color red200 = Color(0xFFFBC3BC);
Color red50 = Color(0xFFFFE3E0);

//orange
Color orange700 = Color(0xFFE45C3A);
Color orange400 = Color(0xFFF3752B);
Color orange200 = Color(0xFFFFC8A8);
Color orange50 = Color(0xFFFFE8DB);

//blue
Color blue700 = Color(0xFF0096C7);
Color blue400 = Color(0xFF00B4D8);
Color blue200 = Color(0xFFCAF0F8);
Color blue50 = Color(0xFFE3FAFF);

//pink
Color pink = Color(0xFFEF5DA8);

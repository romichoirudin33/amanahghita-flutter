import 'package:amanahgitha_flutter/app/utils/colors.dart';
import 'package:flutter/material.dart';

class MyButton extends StatelessWidget {
  const MyButton({
    Key? key,
    this.onPressed,
    required this.text,
    this.color,
  }) : super(key: key);
  final String text;
  final GestureTapCallback? onPressed;
  final Color? color;

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        primary: color ?? logoColor,
        elevation: 0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(6),
        ),
      ),
      onPressed: onPressed,
      child: Text(text),
    );
  }
}

class MyOutlineButton extends StatelessWidget {
  const MyOutlineButton({
    Key? key,
    required this.onPressed,
    required this.text,
    this.color,
  }) : super(key: key);
  final String text;
  final GestureTapCallback onPressed;
  final Color? color;

  @override
  Widget build(BuildContext context) {
    return OutlinedButton(
      style: OutlinedButton.styleFrom(
        primary: color ?? logoColor,
        side: BorderSide(width: 1.0, color: color ?? logoColor),
        elevation: 0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(6),
        ),
      ),
      onPressed: onPressed,
      child: Text(text),
    );
  }
}

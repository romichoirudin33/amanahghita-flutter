import 'package:amanahgitha_flutter/app/utils/colors.dart';
import 'package:flutter/material.dart';

class MyText extends StatelessWidget {
  const MyText({
    Key? key,
    required this.text,
    this.size,
    this.fontWeight,
    this.textAlign,
  }) : super(key: key);

  final String? text;
  final double? size;
  final FontWeight? fontWeight;
  final TextAlign? textAlign;

  @override
  Widget build(BuildContext context) {
    return Text(
      text ?? '',
      textAlign: textAlign,
      style: TextStyle(
        color: gray500,
        fontSize: size ?? 14.0,
        fontWeight: fontWeight ?? FontWeight.w400,
      ),
    );
  }
}

// ignore_for_file: prefer_const_constructors

import 'package:amanahgitha_flutter/app/utils/colors.dart';
import 'package:flutter/material.dart';

class MyAppBar extends StatelessWidget implements PreferredSizeWidget {
  const MyAppBar({
    Key? key,
    this.text,
    this.automaticallyImplyLeading,
    this.actions,
  }) : super(key: key);

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);

  final String? text;
  final bool? automaticallyImplyLeading;
  final List<Widget>? actions;

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: whiteColor,
      automaticallyImplyLeading: automaticallyImplyLeading ?? true,
      elevation: 1,
      iconTheme: IconThemeData(
        color: gray,
      ),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          bottom: Radius.circular(20),
        ),
      ),
      actions: actions,
      title: SizedBox(
        height: 35,
        child: Image.asset("assets/header.png"),
      ),
      // title: Text(
      //   text ?? '',
      //   style: TextStyle(color: gray, fontSize: 15.0),
      // ),
      centerTitle: true,
    );
  }
}

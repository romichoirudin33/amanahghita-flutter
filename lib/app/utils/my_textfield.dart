import 'package:amanahgitha_flutter/app/utils/colors.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';

class MyTextField extends StatelessWidget {
  const MyTextField({
    Key? key,
    this.padding,
    this.hintText,
    this.controller,
    this.height,
    this.keyboardType,
    this.enable,
    this.suffixIcon,
    this.onChanged,
    this.inputFormatters,
    this.onTap,
  }) : super(key: key);

  final double? height;
  final EdgeInsets? padding;
  final String? hintText;
  final TextEditingController? controller;
  final TextInputType? keyboardType;
  final bool? enable;
  final Widget? suffixIcon;
  final ValueChanged<String>? onChanged;
  final List<TextInputFormatter>? inputFormatters;
  final GestureTapCallback? onTap;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: padding,
      height: height,
      child: TextField(
        onTap: onTap,
        controller: controller,
        keyboardType: keyboardType,
        enabled: enable,
        onChanged: onChanged,
        textCapitalization: TextCapitalization.characters,
        inputFormatters: inputFormatters ??
            [
              UpperCaseTextFormatter(),
            ],
        style: TextStyle(
          fontSize: 12.0,
          color: gray500,
        ),
        decoration: InputDecoration(
          label: Text(hintText ?? ''),
          filled: true,
          fillColor: whiteColor,
          suffixIcon: suffixIcon,
          contentPadding: const EdgeInsets.symmetric(horizontal: 12),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: gray500),
            borderRadius: BorderRadius.circular(6.0),
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: gray100),
            borderRadius: BorderRadius.circular(6.0),
          ),
          hintText: hintText,
          hintStyle: TextStyle(
            fontSize: 12.0,
            color: gray400,
          ),
        ),
      ),
    );
  }
}

class MyDateTimeField extends StatelessWidget {
  MyDateTimeField({
    Key? key,
    this.height,
    this.padding,
    this.hintText,
    this.controller,
    this.onChanged,
  }) : super(key: key);

  final format = DateFormat("yyyy-MM-dd");
  final double? height;
  final EdgeInsets? padding;
  final String? hintText;
  final TextEditingController? controller;
  final void Function(DateTime? value)? onChanged;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: padding,
      height: height,
      child: DateTimeField(
        controller: controller,
        onChanged: onChanged,
        decoration: InputDecoration(
          label: Text(
            hintText ?? '',
            style: TextStyle(
              fontSize: 12.0,
              color: gray400,
            ),
          ),
          filled: true,
          fillColor: whiteColor,
          contentPadding: const EdgeInsets.symmetric(horizontal: 12),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: gray500),
            borderRadius: BorderRadius.circular(6.0),
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: gray100),
            borderRadius: BorderRadius.circular(6.0),
          ),
          hintText: hintText,
          hintStyle: TextStyle(
            fontSize: 12.0,
            color: gray400,
          ),
        ),
        format: format,
        onShowPicker: (context, currentValue) {
          return showDatePicker(
              context: context,
              firstDate: DateTime(1900),
              initialDate: currentValue ?? DateTime.now(),
              lastDate: DateTime(2100));
        },
      ),
    );
  }
}

class UpperCaseTextFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    return TextEditingValue(
      text: newValue.text.toUpperCase(),
      selection: newValue.selection,
    );
  }
}

class MoneyTextInputFormatter extends TextInputFormatter {
  final int decimalDigits;

  MoneyTextInputFormatter({this.decimalDigits = 2})
      : assert(decimalDigits >= 0);

  @override
  TextEditingValue formatEditUpdate(
    TextEditingValue oldValue,
    TextEditingValue newValue,
  ) {
    String newText;

    if (decimalDigits == 0) {
      newText = newValue.text.replaceAll(RegExp('[^0-9]'), '');
    } else {
      newText = newValue.text.replaceAll(RegExp('[^0-9.]'), '');
    }

    if (newText.contains('.')) {
      //in case if user's first input is "."
      if (newText.trim() == '.') {
        return newValue.copyWith(
          text: '0.',
          selection: const TextSelection.collapsed(offset: 2),
        );
      }
      //in case if user tries to input multiple "."s or tries to input
      //more than the decimal place
      else if ((newText.split(".").length > 2) ||
          (newText.split(".")[1].length > decimalDigits)) {
        return oldValue;
      } else {
        return newValue;
      }
    }

    //in case if input is empty or zero
    if (newText.trim() == '' || newText.trim() == '0') {
      return newValue.copyWith(text: '');
    } else if (int.parse(newText) < 1) {
      return newValue.copyWith(text: '');
    }

    double newDouble = double.parse(newText);
    var selectionIndexFromTheRight =
        newValue.text.length - newValue.selection.end;

    String newString = NumberFormat("#,##0.##").format(newDouble);

    return TextEditingValue(
      text: newString,
      selection: TextSelection.collapsed(
        offset: newString.length - selectionIndexFromTheRight,
      ),
    );
  }
}

class KtpFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    var text = newValue.text;

    if (newValue.selection.baseOffset == 0) {
      return newValue;
    }

    var buffer = StringBuffer();
    for (int i = 0; i < text.length; i++) {
      buffer.write(text[i]);
      var nonZeroIndex = i + 1;
      if (nonZeroIndex % 4 == 0 && nonZeroIndex != text.length) {
        buffer.write(
            '-'); // Replace this with anything you want to put after each 4 numbers
      }
    }

    var string = buffer.toString();
    return newValue.copyWith(
        text: string,
        selection: TextSelection.collapsed(offset: string.length));
  }
}

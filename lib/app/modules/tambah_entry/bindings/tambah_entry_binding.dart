import 'package:get/get.dart';

import 'package:amanahgitha_flutter/app/modules/tambah_entry/controllers/list_product_controller.dart';
import 'package:amanahgitha_flutter/app/modules/tambah_entry/controllers/data_diri_controller.dart';
import 'package:amanahgitha_flutter/app/modules/tambah_entry/controllers/product_controller.dart';

import '../controllers/tambah_entry_controller.dart';

class TambahEntryBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ListProductController>(
      () => ListProductController(),
    );
    Get.lazyPut<DataDiriController>(
      () => DataDiriController(),
    );
    Get.lazyPut<ProductController>(
      () => ProductController(),
    );
    Get.lazyPut<TambahEntryController>(
      () => TambahEntryController(),
    );
  }
}

import 'package:amanahgitha_flutter/app/data/providers/tambah_entry/data_diri_provider.dart';
import 'package:amanahgitha_flutter/app/modules/tambah_entry/controllers/data_diri_controller.dart';
import 'package:amanahgitha_flutter/app/modules/tambah_entry/controllers/list_product_controller.dart';
import 'package:amanahgitha_flutter/app/modules/tambah_entry/controllers/product_controller.dart';
import 'package:amanahgitha_flutter/app/routes/app_pages.dart';
import 'package:amanahgitha_flutter/app/utils/colors.dart';
import 'package:amanahgitha_flutter/app/utils/snackbar.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class TambahEntryController extends GetxController {
  final ListProductController listProductController = Get.find();
  final ProductController productController = Get.find();
  final DataDiriController dataDiriController = Get.find();

  final isLoading = false.obs;
  final isEnable = false.obs;

  submitMember() async {
    try {
      isLoading.value = true;
      if (dataDiriController.namaPesertaEditingController.text
          .toString()
          .isEmpty) {
        snackbar(
          title: "Gagal",
          message: "Nama Peserta harus di isi",
          color: dangerBorderColor,
        );
      } else if (dataDiriController.noTandaPengenalEditingController.text
          .toString()
          .isEmpty) {
        snackbar(
          title: "Gagal",
          message: "No Tanda pengenal harus di isi",
          color: dangerBorderColor,
        );
      } else {
        String noTandaPengenal = '';
        if (dataDiriController.jenisTandaPengenalSelected.value == "Paspor") {
          noTandaPengenal = dataDiriController
              .noTandaPengenalEditingController.text
              .toString();
        } else {
          noTandaPengenal = dataDiriController
              .noTandaPengenalEditingController.text
              .toString()
              .replaceAll('-', '');
        }

        var params = {
          'pengguna': GetStorage().read('pengguna'),
          'policy_no': Get.arguments['no_policy'].toString(),
          'benefit_id': listProductController.benefitIdSelected.value,
          'spa_no_group': Get.arguments['spa_no_group'].toString(),
          'nama':
              dataDiriController.namaPesertaEditingController.text.toString(),
          'tanggal_lahir': listProductController
              .tanggalLahirEditingController.text
              .toString(),
          'usia': listProductController.umurEditingController.text.toString(),
          'jenis_kelamin': dataDiriController.jenisKelaminSelected.value[0],
          'from_date':
              listProductController.awalKontrakController.text.toString(),
          'thru_date': listProductController.akhirKontrak.value,
          'keterangan':
              dataDiriController.keteranganEditingController.text.toString(),
          'premi': listProductController.premi.value.replaceAll(",", ""),
          'benefit': listProductController.benefitController.text
              .toString()
              .replaceAll(",", ""),
          'underwriting_status': listProductController.uwStatus.value,
          'status': 0,
          'contract_period_year': listProductController
              .contractPeriodYearController.text
              .toString(),
          'contract_period_month': listProductController
              .contractPeriodMonthController.text
              .toString(),
          'tanda_pengenal': dataDiriController.jenisTandaPengenalSelected.value,
          'no_tanda_pengenal': noTandaPengenal,
          'ktp_alamat':
              dataDiriController.alamatEditingController.text.toString(),
          'ktp_kota': dataDiriController.kotaEditingController.text.toString(),
          'ktp_kode_pos':
              dataDiriController.kodePosEditingController.text.toString(),
          'ktp_telephone':
              dataDiriController.teleponEditingController.text.toString(),
          'ktp_handphone':
              dataDiriController.selulerEditingController.text.toString(),
          'rate': listProductController.rate.value,
          'tabarru': listProductController.tabarru.value,
          'no_batch':
              dataDiriController.nomorBatchEditingController.text.toString(),
          'tanggal_batch':
              dataDiriController.tanggalBatchEditingController.text.toString(),
          'no_referensi':
              productController.cabangSelected.value.substring(0, 4),
        };

        bool? data = await DataDiriProvider().submitMember(params);
        if (data != null) {
          if (data) {
            snackbar(
              title: "Info",
              message: "Data berhasil di submit",
            );
            Get.offAllNamed(Routes.HOME);
          } else {
            snackbar(
              title: "Info",
              message: "Data gagal di submit",
              color: dangerBorderColor,
            );
          }
        } else {
          snackbar(
            title: "Warning",
            message: "Terjadi kesalahan, silahkan coba kembali",
            color: warningBorderColor,
          );
        }
      }
    } finally {
      isLoading.value = false;
    }
  }
}

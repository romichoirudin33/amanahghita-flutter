import 'package:amanahgitha_flutter/app/data/models/reference.dart';
import 'package:amanahgitha_flutter/app/data/providers/tambah_entry/data_diri_provider.dart';
import 'package:amanahgitha_flutter/app/utils/colors.dart';
import 'package:amanahgitha_flutter/app/utils/snackbar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class DataDiriController extends GetxController {
  final selected = ''.obs;

  final jenisKelamin = <DetailRefrence>[].obs;
  final jenisKelaminList = <String>[].obs;

  final jenisTandaPengenal = <DetailRefrence>[].obs;
  final jenisTandaPengenalList = <String>[].obs;

  final namaPesertaEditingController = TextEditingController();
  final jenisKelaminSelected = 'Pria'.obs;
  final jenisTandaPengenalSelected = 'KTP'.obs;
  final noTandaPengenalEditingController = TextEditingController();
  final alamatEditingController = TextEditingController();
  final kotaEditingController = TextEditingController();
  final kodePosEditingController = TextEditingController();
  final teleponEditingController = TextEditingController();
  final selulerEditingController = TextEditingController();
  final beratBadanEditingController = TextEditingController();
  final tinggiBadanEditingController = TextEditingController();
  final bmiEditingController = TextEditingController();
  final keteranganEditingController = TextEditingController();
  final nomorBatchEditingController = TextEditingController();
  final tanggalBatchEditingController = TextEditingController();

  getJenisKelamin(Map<String, dynamic> params) async {
    var data = await DataDiriProvider().getReference(params);
    if (data != null) {
      data = data;
    } else {
      data = [
        DetailRefrence(
          id: 'ff8f2ff659564677adf2d54e06bf9045',
          kode: 'P',
          keterangan: "Pria",
        )
      ];
    }

    jenisKelamin.value = data;

    jenisKelaminList.clear();
    for (var e in data) {
      jenisKelaminList.add(e.keterangan.toString());
    }
    jenisKelaminSelected.value = jenisKelamin[0].keterangan.toString();
  }

  getTandaPengenal(Map<String, dynamic> params) async {
    var data = await DataDiriProvider().getReference(params);
    if (data != null) {
      data = data;
    } else {
      data = [
        DetailRefrence(
          id: '7b5229d2c4d84098842c73edf03b4761',
          kode: 'KTP',
          keterangan: "KTP",
        )
      ];
    }

    jenisTandaPengenal.value = data;

    jenisTandaPengenalList.clear();
    for (var e in data) {
      jenisTandaPengenalList.add(e.keterangan.toString());
    }
    jenisTandaPengenalSelected.value =
        jenisTandaPengenal[0].keterangan.toString();
  }

  checkBmi() async {
    if (beratBadanEditingController.text.isEmpty ||
        tinggiBadanEditingController.text.isEmpty) {
      snackbar(
        title: "Info",
        message: "Isi terlebih dahulu berat dan tinggi badan",
        color: dangerBorderColor,
      );
    } else {
      var params = {
        'pengguna': GetStorage().read('pengguna'),
        'weight': beratBadanEditingController.text.toString(),
        'height': tinggiBadanEditingController.text.toString(),
      };
      String? data = await DataDiriProvider().getBmi(params);
      bmiEditingController.text = data ?? '-';
    }
  }

  @override
  void onInit() async {
    super.onInit();
    await getJenisKelamin(
        {'pengguna': GetStorage().read('pengguna'), 'reference_code': '5'});
    await getTandaPengenal(
        {'pengguna': GetStorage().read('pengguna'), 'reference_code': '1'});
  }
}

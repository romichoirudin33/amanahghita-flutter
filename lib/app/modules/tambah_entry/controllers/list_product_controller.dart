import 'package:amanahgitha_flutter/app/data/models/list_product.dart';
import 'package:amanahgitha_flutter/app/data/providers/tambah_entry/product_provider.dart';
import 'package:amanahgitha_flutter/app/utils/colors.dart';
import 'package:amanahgitha_flutter/app/utils/snackbar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:intl/intl.dart';

class ListProductController extends GetxController {
  final productList = <Product>[].obs;
  final productIdSelected = "".obs;
  final benefitIdSelected = "".obs;

  final rate = '-'.obs;
  final flag = '-'.obs;
  final premi = '-'.obs;
  final totalBenefit = '-'.obs;
  final uwStatus = '-'.obs;
  final paymentMethod = '-'.obs;
  final tabarru = '-'.obs;
  final status = false.obs;

  final benefitController = TextEditingController();
  // final premiController = TextEditingController();
  // final rateController = TextEditingController();
  // final flagController = TextEditingController();
  // final totalBenefitController = TextEditingController();
  // final uwStatusController = TextEditingController();
  // final paymentMethodsController = TextEditingController();

  final tanggalLahirEditingController = TextEditingController();
  final umurEditingController = TextEditingController();
  final awalKontrakController = TextEditingController();
  final contractPeriodYearController = TextEditingController();
  final contractPeriodMonthController = TextEditingController();
  final contractPeriodDayController = TextEditingController();
  final akhirKontrak = '-'.obs;
  // final akhirKontrakController = TextEditingController();

  final isLoading = false.obs;
  final isLoadingCalculate = false.obs;

  getProduct(Map<String, dynamic> params) async {
    try {
      isLoading.value = true;
      var data = await ProductProvider().getProductBenefits(params);
      if (data != null) {
        data = data;
      } else {
        data = [];
      }
      productList.value = data;
    } finally {
      isLoading.value = false;
    }
  }

  checkUmur() async {
    if (tanggalLahirEditingController.text.isNotEmpty &&
        awalKontrakController.text.isNotEmpty) {
      var params = {
        'pengguna': GetStorage().read('pengguna'),
        'tgl_awal': tanggalLahirEditingController.text.toString(),
        'tgl_akhir': awalKontrakController.text.toString(),
      };
      String? umur = await ProductProvider().getAge(params);
      umurEditingController.text = umur ?? '';
    }
  }

  checkPaymentMethod() async {
    var params = {
      'pengguna': GetStorage().read('pengguna'),
      'policy_no': Get.arguments['no_policy'].toString(),
    };
    String? data = await ProductProvider().getPaymentMethod(params);
    paymentMethod.value = data ?? '-';
  }

  calculate() async {
    try {
      isLoadingCalculate.value = true;
      if (benefitIdSelected.value == '') {
        snackbar(
          title: "Info",
          message: "Tidak ada produk yang di pilih",
          color: dangerBorderColor,
        );
      } else if (benefitController.text.isEmpty) {
        snackbar(
          title: "Info",
          message: "Benefit masih kosong",
          color: dangerBorderColor,
        );
      } else if (tanggalLahirEditingController.text.isEmpty ||
          awalKontrakController.text.isEmpty ||
          contractPeriodYearController.text.isEmpty ||
          contractPeriodMonthController.text.isEmpty) {
        snackbar(
          title: "Gagal",
          message:
              "Tanggal lahir, awal kontrak dan masa asuransi harus di isi !",
          color: dangerBorderColor,
        );
      } else {
        var params = {
          'pengguna': GetStorage().read('pengguna'),
          'policy_no': Get.arguments['no_policy'].toString(),
          'benefit_id': benefitIdSelected.value,
          'product_id': productIdSelected.value,
          'contract_year': contractPeriodYearController.text.toString(),
          'contract_month': contractPeriodMonthController.text.toString(),
          'age': umurEditingController.text.toString(),
          'benefit': benefitController.text.toString().replaceAll(",", ""),
        };
        var data = await ProductProvider().getCalculate(params);
        if (data != null) {
          rate.value = data['premi_rate'].toString();
          flag.value = data['flag'].toString();
          premi.value = NumberFormat.decimalPattern()
              .format(double.parse(data['premi'].toString()))
              .toString();
          totalBenefit.value = NumberFormat.decimalPattern()
              .format(double.parse(data['total_benefit'].toString()))
              .toString();
          uwStatus.value = data['uw_status'].toString();
          tabarru.value = data['tabarru'].toString();
          status.value = data['status'];

          if (data['status'] == false) {
            snackbar(
              title: "Status False",
              message: data['keterangan'].toString(),
              color: warningBorderColor,
            );
          }
        }

        var params2 = {
          'pengguna': GetStorage().read('pengguna'),
          'tgl_awal_kontrak': awalKontrakController.text.toString(),
          'tahun': contractPeriodYearController.text.toString(),
          'bulan': contractPeriodMonthController.text.toString(),
          'hari': contractPeriodDayController.text.toString(),
        };
        String? akhir = await ProductProvider().getEndOfContract(params2);
        akhirKontrak.value = akhir ?? '-';
      }
    } finally {
      isLoadingCalculate.value = false;
    }
  }

  @override
  void onInit() async {
    super.onInit();
    await getProduct({
      'pengguna': GetStorage().read('pengguna'),
      'policy_no': Get.arguments['no_policy'].toString()
    });
    checkPaymentMethod();
    contractPeriodYearController.text = 0.toString();
    contractPeriodMonthController.text = 0.toString();
    contractPeriodDayController.text = 0.toString();
  }
}

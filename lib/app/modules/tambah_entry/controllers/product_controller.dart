import 'package:amanahgitha_flutter/app/data/models/bsi.dart';
import 'package:amanahgitha_flutter/app/data/providers/tambah_entry/product_provider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class ProductController extends GetxController {
  final selected = ''.obs;

  final cabang = <Cabang>[].obs;
  final cabangList = <Cabang>[].obs;
  final cabangSelected = '0000'.obs;

  final searchEditingController = TextEditingController();
  final isLoading = false.obs;

  getListCabang(Map<String, dynamic> params) async {
    var data = await ProductProvider().getListCabang(params);
    if (data != null) {
      data = data;
    } else {
      data = [
        Cabang(
          kodeCabang: '',
          namaCabang: '',
        )
      ];
    }

    cabang.value = data;
    cabangList.value = data;
    cabangSelected.value =
        "${cabang[0].kodeCabang.toString()} - ${cabang[0].namaCabang.toString()}";
  }

  getFilter(String value) {
    if (value.isNotEmpty) {
      List<Cabang> cabangTemp = <Cabang>[];
      for (var element in cabang) {
        String? nama = element.namaCabang;
        if (nama!.contains(value)) {
          cabangTemp.add(element);
        }
      }
      cabangList.value = cabangTemp;
    } else {
      cabangList.value = cabang;
    }
  }

  @override
  void onInit() async {
    super.onInit();
    await getListCabang({'pengguna': GetStorage().read('pengguna')});
  }
}

import 'package:amanahgitha_flutter/app/data/models/bsi.dart';
import 'package:amanahgitha_flutter/app/modules/tambah_entry/controllers/product_controller.dart';
import 'package:amanahgitha_flutter/app/modules/tambah_entry/views/list_product_view.dart';
import 'package:amanahgitha_flutter/app/utils/colors.dart';
import 'package:amanahgitha_flutter/app/utils/loading.dart';
import 'package:amanahgitha_flutter/app/utils/my_textfield.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

class ProdukView extends GetView<ProductController> {
  const ProdukView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Padding(
          padding: EdgeInsets.only(bottom: 4),
          child: Text(
            'Nama/Kode Cabang',
          ),
        ),
        InkWell(
          onTap: () {
            Get.bottomSheet(
              Container(
                padding: const EdgeInsets.symmetric(
                  horizontal: 20,
                  vertical: 20,
                ),
                child: Wrap(
                  children: [
                    Center(
                      child: Container(
                        margin: const EdgeInsets.only(bottom: 15),
                        height: 4,
                        width: 80,
                        color: Colors.black54,
                      ),
                    ),
                    const Padding(
                      padding: EdgeInsets.only(bottom: 10),
                      child: Text(
                        'Kode / Nama Cabang',
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.symmetric(vertical: 8),
                      height: Get.mediaQuery.size.height / 2,
                      width: Get.mediaQuery.size.width,
                      child: Column(
                        children: [
                          MyTextField(
                            controller: controller.searchEditingController,
                            onChanged: (value) {
                              controller.getFilter(value);
                            },
                            hintText: 'Cari ...',
                            padding: const EdgeInsets.symmetric(vertical: 4),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Obx(
                            () => controller.isLoading.value
                                ? LoadingShimmer(
                                    width: Get.mediaQuery.size.width / 3,
                                    height: 10,
                                  )
                                : Expanded(
                                    child: ListView.separated(
                                      separatorBuilder: (context, index) =>
                                          const SizedBox(
                                        height: 10,
                                      ),
                                      scrollDirection: Axis.vertical,
                                      itemCount: controller.cabangList.length,
                                      itemBuilder:
                                          (BuildContext context, int index) {
                                        return InkWell(
                                          onTap: () {
                                            controller.cabangSelected.value =
                                                "${controller.cabangList[index].kodeCabang} - ${controller.cabangList[index].namaCabang}";
                                            Get.back();
                                          },
                                          child: Item(
                                            item: controller.cabangList[index],
                                          ),
                                        );
                                      },
                                    ),
                                  ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
              backgroundColor: whiteColor,
              isDismissible: true,
              enableDrag: true,
              shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20),
                  topRight: Radius.circular(20),
                ),
              ),
            );
          },
          child: Container(
            width: Get.mediaQuery.size.width,
            height: 45,
            margin: const EdgeInsets.only(bottom: 4),
            padding: const EdgeInsets.symmetric(horizontal: 10),
            decoration: BoxDecoration(
              color: whiteColor,
              borderRadius: BorderRadius.circular(6.0),
              border: Border.all(
                color: gray400,
              ),
            ),
            child: Obx(
              () => Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(controller.cabangSelected.value),
                ],
              ),
            ),
          ),
        ),
        Row(
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Padding(
                    padding: EdgeInsets.only(bottom: 4),
                    child: Text(
                      'Nomor Policy',
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 4),
                    child: Text(
                      Get.arguments['no_policy'].toString(),
                      style: TextStyle(color: gray500),
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Padding(
                    padding: EdgeInsets.only(bottom: 4),
                    child: Text(
                      'Spa No Group',
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 4),
                    child: Text(
                      Get.arguments['spa_no_group'].toString(),
                      style: TextStyle(color: gray500),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
        Container(
          padding: const EdgeInsets.only(bottom: 4, top: 4),
          child: ListProductState(),
        ),
      ],
    );
  }
}

class Item extends StatelessWidget {
  Item({Key? key, required this.item}) : super(key: key);

  final Cabang item;

  final Color selectedColor = green400;
  final Color unSelectedColor = gray;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.mediaQuery.size.width,
      padding: const EdgeInsets.symmetric(horizontal: 14.0, vertical: 8),
      decoration: BoxDecoration(
        color: whiteColor,
        border: Border(
          left: BorderSide(
            color: unSelectedColor,
            width: 3.0,
          ),
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            item.namaCabang?.toUpperCase() ?? '-',
            style: TextStyle(color: unSelectedColor, fontSize: 12),
          ),
          Text(
            item.kodeCabang ?? '',
            style: TextStyle(color: gray, fontSize: 12),
          ),
        ],
      ),
    );
  }
}

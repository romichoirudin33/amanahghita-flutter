import 'package:amanahgitha_flutter/app/modules/tambah_entry/controllers/data_diri_controller.dart';
import 'package:amanahgitha_flutter/app/modules/tambah_entry/controllers/tambah_entry_controller.dart';
import 'package:amanahgitha_flutter/app/utils/colors.dart';
import 'package:amanahgitha_flutter/app/utils/loading.dart';
import 'package:amanahgitha_flutter/app/utils/my_textfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:get/get.dart';

class DataDiriView extends GetView<DataDiriController> {
  DataDiriView({Key? key}) : super(key: key);

  final TambahEntryController tambahEntryController = Get.find();

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        MyTextField(
          controller: controller.namaPesertaEditingController,
          hintText: '* Nama Peserta',
          padding: const EdgeInsets.symmetric(vertical: 4),
        ),
        const Padding(
          padding: EdgeInsets.only(bottom: 4, top: 4),
          child: Text(
            '* Jenis Kelamin',
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 4.0),
          child: Obx(
            () => controller.jenisKelaminList.isNotEmpty
                ? InputDecorator(
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: whiteColor,
                      contentPadding: const EdgeInsets.symmetric(
                        horizontal: 12,
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: gray500),
                        borderRadius: BorderRadius.circular(6.0),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: gray100),
                        borderRadius: BorderRadius.circular(6.0),
                      ),
                      hintText: 'Jenis Kelamin',
                    ),
                    child: DropdownButtonHideUnderline(
                      child: Obx(
                        () => DropdownButton<String>(
                          value: controller.jenisKelaminSelected.value,
                          icon: const Icon(Icons.arrow_drop_down),
                          style: TextStyle(
                            fontSize: 12.0,
                            color: gray500,
                          ),
                          onChanged: (String? newValue) {
                            if (newValue != null) {
                              controller.jenisKelaminSelected.value = newValue;
                              tambahEntryController.isEnable.value = false;
                            }
                          },
                          items: controller.jenisKelaminList
                              .map<DropdownMenuItem<String>>((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                        ),
                      ),
                    ),
                  )
                : LoadingShimmer(
                    width: Get.mediaQuery.size.width / 3,
                    height: 10,
                  ),
          ),
        ),
        const Padding(
          padding: EdgeInsets.only(bottom: 4, top: 4),
          child: Text(
            '* Tanda Pengenal',
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 4.0),
          child: Obx(
            () => controller.jenisKelaminList.isNotEmpty
                ? InputDecorator(
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: whiteColor,
                      contentPadding: const EdgeInsets.symmetric(
                        horizontal: 12,
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: gray500),
                        borderRadius: BorderRadius.circular(6.0),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: gray100),
                        borderRadius: BorderRadius.circular(6.0),
                      ),
                      hintText: 'Tanda Pengenal',
                    ),
                    child: DropdownButtonHideUnderline(
                      child: Obx(
                        () => DropdownButton<String>(
                          value: controller.jenisTandaPengenalSelected.value,
                          icon: const Icon(Icons.arrow_drop_down),
                          style: TextStyle(
                            fontSize: 12.0,
                            color: gray500,
                          ),
                          onChanged: (String? newValue) {
                            if (newValue != null) {
                              controller.jenisTandaPengenalSelected.value =
                                  newValue;
                              controller.noTandaPengenalEditingController.text =
                                  '';
                            }
                          },
                          items: controller.jenisTandaPengenalList
                              .map<DropdownMenuItem<String>>((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                        ),
                      ),
                    ),
                  )
                : LoadingShimmer(
                    width: Get.mediaQuery.size.width / 3,
                    height: 10,
                  ),
          ),
        ),
        Obx(
          () => MyTextField(
            controller: controller.noTandaPengenalEditingController,
            inputFormatters:
                controller.jenisTandaPengenalSelected.value == "Paspor"
                    ? null
                    : [FilteringTextInputFormatter.digitsOnly, KtpFormatter()],
            keyboardType:
                controller.jenisTandaPengenalSelected.value == "Paspor"
                    ? TextInputType.text
                    : TextInputType.number,
            hintText: '* No Tanda Pengenal',
            padding: const EdgeInsets.symmetric(vertical: 4),
          ),
        ),
        MyTextField(
          controller: controller.alamatEditingController,
          hintText: 'Alamat',
          padding: const EdgeInsets.symmetric(vertical: 4),
        ),
        MyTextField(
          controller: controller.kotaEditingController,
          hintText: 'Kota',
          padding: const EdgeInsets.symmetric(vertical: 4),
        ),
        MyTextField(
          keyboardType: TextInputType.number,
          controller: controller.kodePosEditingController,
          hintText: 'Kode Pos',
          padding: const EdgeInsets.symmetric(vertical: 4),
        ),
        Row(
          children: [
            Expanded(
              child: MyTextField(
                hintText: 'Telepon',
                controller: controller.teleponEditingController,
                keyboardType: TextInputType.phone,
                padding: const EdgeInsets.symmetric(vertical: 4),
              ),
            ),
            Expanded(
              child: MyTextField(
                hintText: 'Seluler',
                controller: controller.selulerEditingController,
                keyboardType: TextInputType.phone,
                padding: const EdgeInsets.symmetric(vertical: 4),
              ),
            ),
          ],
        ),
        Row(
          children: [
            Expanded(
              child: MyTextField(
                hintText: 'Berat Badan (Kg)',
                keyboardType: TextInputType.number,
                controller: controller.beratBadanEditingController,
                padding: const EdgeInsets.symmetric(vertical: 4),
              ),
            ),
            Expanded(
              child: MyTextField(
                hintText: 'Tinggi Badan (cm)',
                keyboardType: TextInputType.number,
                controller: controller.tinggiBadanEditingController,
                padding: const EdgeInsets.symmetric(vertical: 4),
              ),
            ),
          ],
        ),
        MyTextField(
          controller: controller.bmiEditingController,
          hintText: 'BMI',
          suffixIcon: IconButton(
            icon: const Icon(Icons.refresh),
            onPressed: () {
              controller.checkBmi();
            },
          ),
          padding: const EdgeInsets.symmetric(vertical: 4),
        ),
        MyTextField(
          controller: controller.keteranganEditingController,
          hintText: 'Keterangan',
          padding: const EdgeInsets.symmetric(vertical: 4),
        ),
        MyTextField(
          controller: controller.nomorBatchEditingController,
          hintText: 'Nomor Batch',
          padding: const EdgeInsets.symmetric(vertical: 4),
        ),
        MyDateTimeField(
          controller: controller.tanggalBatchEditingController,
          hintText: 'Tanggal Batch',
          padding: const EdgeInsets.symmetric(vertical: 4),
        ),
      ],
    );
  }
}

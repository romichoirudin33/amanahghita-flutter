// ignore_for_file: must_be_immutable

import 'package:amanahgitha_flutter/app/data/models/list_product.dart';
import 'package:amanahgitha_flutter/app/modules/home/views/menu_batal/list_loading.dart';
import 'package:amanahgitha_flutter/app/modules/tambah_entry/controllers/list_product_controller.dart';
import 'package:amanahgitha_flutter/app/modules/tambah_entry/controllers/tambah_entry_controller.dart';
import 'package:amanahgitha_flutter/app/utils/buttons.dart';
import 'package:amanahgitha_flutter/app/utils/colors.dart';
import 'package:amanahgitha_flutter/app/utils/empty_widget.dart';
import 'package:amanahgitha_flutter/app/utils/my_textfield.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ListProductState extends GetView<ListProductController> {
  ListProductState({Key? key}) : super(key: key);

  TambahEntryController tambahEntryController = Get.find();

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Padding(
          padding: EdgeInsets.only(bottom: 4, top: 4),
          child: Text(
            'Nama Produk',
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 8),
          child: Text(
            Get.arguments['product_name'].toString(),
            style: TextStyle(color: gray500),
          ),
        ),
        SizedBox(
          height: 200,
          child: Obx(
            () => controller.isLoading.value
                ? const ListLoading()
                : controller.productList.isNotEmpty
                    ? ListProduct()
                    : const Expanded(
                        child: EmptyWidget(msg: 'Tidak terdapat data'),
                      ),
          ),
        ),
        MyTextField(
          controller: controller.benefitController,
          keyboardType: TextInputType.number,
          inputFormatters: [MoneyTextInputFormatter()],
          hintText: '*Benefit',
          padding: const EdgeInsets.symmetric(vertical: 4),
        ),
        Row(
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Padding(
                    padding: EdgeInsets.only(bottom: 4, top: 4),
                    child: Text(
                      'Rate',
                    ),
                  ),
                  Obx(
                    () => Padding(
                      padding: const EdgeInsets.only(bottom: 8),
                      child: Text(
                        controller.rate.value,
                        style: TextStyle(color: gray500),
                      ),
                    ),
                  ),
                  const Padding(
                    padding: EdgeInsets.only(bottom: 4, top: 4),
                    child: Text(
                      'Premi',
                    ),
                  ),
                  Obx(
                    () => Padding(
                      padding: const EdgeInsets.only(bottom: 8),
                      child: Text(
                        controller.premi.value,
                        style: TextStyle(color: gray500),
                      ),
                    ),
                  ),
                  const Padding(
                    padding: EdgeInsets.only(bottom: 4, top: 4),
                    child: Text(
                      'Underwriting Status',
                    ),
                  ),
                  Obx(
                    () => Padding(
                      padding: const EdgeInsets.only(bottom: 8),
                      child: Text(
                        controller.uwStatus.value,
                        style: TextStyle(color: gray500),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Padding(
                    padding: EdgeInsets.only(bottom: 4, top: 4),
                    child: Text(
                      'Cons. Flag',
                    ),
                  ),
                  Obx(
                    () => Padding(
                      padding: const EdgeInsets.only(bottom: 8),
                      child: Text(
                        controller.flag.value,
                        style: TextStyle(color: gray500),
                      ),
                    ),
                  ),
                  const Padding(
                    padding: EdgeInsets.only(bottom: 4, top: 4),
                    child: Text(
                      'Total Benefit',
                    ),
                  ),
                  Obx(
                    () => Padding(
                      padding: const EdgeInsets.only(bottom: 8),
                      child: Text(
                        controller.totalBenefit.value,
                        style: TextStyle(color: gray500),
                      ),
                    ),
                  ),
                  const Padding(
                    padding: EdgeInsets.only(bottom: 4, top: 4),
                    child: Text(
                      'Payment Methods',
                    ),
                  ),
                  Obx(
                    () => Padding(
                      padding: const EdgeInsets.only(bottom: 8),
                      child: Text(
                        controller.paymentMethod.value,
                        style: TextStyle(color: gray500),
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
        Row(
          children: [
            Expanded(
              flex: 3,
              child: MyDateTimeField(
                controller: controller.tanggalLahirEditingController,
                onChanged: (value) {
                  controller.checkUmur();
                  tambahEntryController.isEnable.value = false;
                },
                hintText: '* Tanggal Lahir',
              ),
            ),
            Expanded(
              flex: 1,
              child: MyTextField(
                controller: controller.umurEditingController,
                hintText: '* Umur',
                enable: false,
                padding: const EdgeInsets.symmetric(vertical: 4),
              ),
            ),
          ],
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 4, top: 12),
          child: MyDateTimeField(
            controller: controller.awalKontrakController,
            onChanged: (value) {
              controller.checkUmur();
              tambahEntryController.isEnable.value = false;
            },
            hintText: '* Awal Kontrak',
          ),
        ),
        const Padding(
          padding: EdgeInsets.only(bottom: 4, top: 4),
          child: Text(
            'Masa Asuransi',
          ),
        ),
        Row(
          children: [
            Expanded(
              child: MyTextField(
                controller: controller.contractPeriodYearController,
                onChanged: (value) {
                  tambahEntryController.isEnable.value = false;
                },
                keyboardType: TextInputType.number,
                hintText: '* Tahun',
                padding: const EdgeInsets.symmetric(vertical: 4),
              ),
            ),
            Expanded(
              child: MyTextField(
                controller: controller.contractPeriodMonthController,
                onChanged: (value) {
                  tambahEntryController.isEnable.value = false;
                },
                keyboardType: TextInputType.number,
                hintText: '* Bulan',
                padding: const EdgeInsets.symmetric(vertical: 4),
              ),
            ),
            Expanded(
              child: MyTextField(
                controller: controller.contractPeriodDayController,
                onChanged: (value) {
                  tambahEntryController.isEnable.value = false;
                },
                keyboardType: TextInputType.number,
                hintText: '* Hari',
                padding: const EdgeInsets.symmetric(vertical: 4),
              ),
            ),
          ],
        ),
        const Padding(
          padding: EdgeInsets.only(bottom: 4, top: 4),
          child: Text(
            'Akhir Kontrak',
          ),
        ),
        Obx(
          () => Padding(
            padding: const EdgeInsets.only(bottom: 8),
            child: Text(
              controller.akhirKontrak.value,
              style: TextStyle(color: gray500),
            ),
          ),
        ),
        Obx(() {
          return controller.isLoadingCalculate.value
              ? const SizedBox(
                  height: 40,
                  child: CircularProgressIndicator(),
                )
              : MyButton(
                  onPressed: () {
                    controller.calculate();
                    tambahEntryController.isEnable.value = true;
                  },
                  text: 'Proses Kalkulasi',
                );
        }),
      ],
    );
  }
}

class ListProduct extends GetView<ListProductController> {
  ListProduct({Key? key}) : super(key: key);

  final TambahEntryController tambahEntryController = Get.find();

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      separatorBuilder: (context, index) => const SizedBox(
        height: 10,
      ),
      scrollDirection: Axis.vertical,
      itemCount: controller.productList.length,
      itemBuilder: (BuildContext ctx, int index) {
        return InkWell(
          onTap: () {
            if (controller.benefitIdSelected.value !=
                controller.productList[index].benefitId) {
              controller.benefitIdSelected.value =
                  controller.productList[index].benefitId ?? '';
              controller.productIdSelected.value =
                  controller.productList[index].productId ?? '';
            }
            tambahEntryController.isEnable.value = false;
          },
          child: ItemProduct(
            item: controller.productList[index],
          ),
        );
      },
    );
  }
}

class ItemProduct extends GetView<ListProductController> {
  ItemProduct({Key? key, required this.item}) : super(key: key);

  final Product item;

  Color selectedColor = green400;
  Color unSelectedColor = gray;

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Container(
        width: Get.mediaQuery.size.width,
        height: 56.0,
        padding: const EdgeInsets.symmetric(horizontal: 14.0),
        decoration: BoxDecoration(
          color: whiteColor,
          border: Border(
            left: BorderSide(
              color: controller.benefitIdSelected.value == item.benefitId
                  ? selectedColor
                  : unSelectedColor,
              width: 3.0,
            ),
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    item.benefitsFlag ?? '',
                    style: TextStyle(color: gray, fontSize: 10),
                  ),
                  Text(
                    item.description ?? '',
                    style: TextStyle(
                        color:
                            controller.benefitIdSelected.value == item.benefitId
                                ? selectedColor
                                : unSelectedColor,
                        fontSize: 12),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Text(
                    item.spaNoGroup ?? '',
                    style: TextStyle(color: gray, fontSize: 10),
                  ),
                  Text(
                    '',
                    textAlign: TextAlign.end,
                    style: TextStyle(color: gray500, fontSize: 12),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

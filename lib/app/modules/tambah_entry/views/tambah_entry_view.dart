// ignore_for_file: must_be_immutable

import 'package:amanahgitha_flutter/app/modules/tambah_entry/controllers/list_product_controller.dart';
import 'package:amanahgitha_flutter/app/modules/tambah_entry/views/data_diri_view.dart';
import 'package:amanahgitha_flutter/app/modules/tambah_entry/views/produk_view.dart';
import 'package:amanahgitha_flutter/app/utils/app_bar.dart';
import 'package:amanahgitha_flutter/app/utils/buttons.dart';
import 'package:amanahgitha_flutter/app/utils/colors.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/tambah_entry_controller.dart';

class TambahEntryView extends GetView<TambahEntryController> {
  TambahEntryView({Key? key}) : super(key: key);

  ListProductController listProductController = Get.find();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const MyAppBar(
        text: "Tambah Entry",
        automaticallyImplyLeading: true,
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(8),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const ProdukView(),
            const SizedBox(height: 20),
            DataDiriView(),
            const SizedBox(height: 20),
            Text(
              "* wajib di isi",
              style: TextStyle(
                color: dangerColor,
              ),
            ),
            const SizedBox(height: 20),
            SizedBox(
              width: Get.mediaQuery.size.width,
              child: Obx(
                () => controller.isEnable.value &&
                        listProductController.status.value &&
                        !controller.isLoading.value
                    ? MyButton(
                        onPressed: () {
                          controller.submitMember();
                        },
                        text: 'Tambah',
                      )
                    : const MyButton(
                        onPressed: null,
                        text: 'Tambah',
                      ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

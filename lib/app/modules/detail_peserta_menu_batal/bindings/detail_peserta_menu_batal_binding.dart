import 'package:get/get.dart';

import '../controllers/detail_peserta_menu_batal_controller.dart';

class DetailPesertaMenuBatalBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DetailPesertaMenuBatalController>(
      () => DetailPesertaMenuBatalController(),
    );
  }
}

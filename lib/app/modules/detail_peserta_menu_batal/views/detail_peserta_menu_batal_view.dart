import 'package:amanahgitha_flutter/app/utils/app_bar.dart';
import 'package:amanahgitha_flutter/app/utils/typhography.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../controllers/detail_peserta_menu_batal_controller.dart';

class DetailPesertaMenuBatalView
    extends GetView<DetailPesertaMenuBatalController> {
  const DetailPesertaMenuBatalView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const MyAppBar(
        text: 'Detail Peserta',
        automaticallyImplyLeading: true,
      ),
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Expanded(
                  child: MyText(
                    text: "No Peserta",
                  ),
                ),
                Expanded(
                  child: MyText(
                    text: Get.arguments['no_peserta'].toString(),
                    fontWeight: FontWeight.bold,
                    textAlign: TextAlign.right,
                  ),
                )
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Expanded(
                  child: MyText(
                    text: "Nama Peserta",
                  ),
                ),
                Expanded(
                  child: MyText(
                    text: Get.arguments['nama'].toString(),
                    fontWeight: FontWeight.bold,
                    textAlign: TextAlign.right,
                  ),
                )
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Expanded(
                  child: MyText(
                    text: "Tgl Lahir",
                  ),
                ),
                Expanded(
                  child: MyText(
                    text: Get.arguments['tanggal_lahir'].toString(),
                    fontWeight: FontWeight.bold,
                    textAlign: TextAlign.right,
                  ),
                )
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Expanded(
                  child: MyText(
                    text: "Awal Kontrak",
                  ),
                ),
                Expanded(
                  child: MyText(
                    text:
                        "${Get.arguments['from_date'].toString().substring(0, 4)}-${Get.arguments['from_date'].toString().substring(5, 7)}-${Get.arguments['from_date'].toString().substring(8, 10)}",
                    fontWeight: FontWeight.bold,
                    textAlign: TextAlign.right,
                  ),
                )
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Expanded(
                  child: MyText(
                    text: "Akhir Kontrak",
                  ),
                ),
                Expanded(
                  child: MyText(
                    text:
                        "${Get.arguments['thru_date'].toString().substring(0, 4)}-${Get.arguments['thru_date'].toString().substring(5, 7)}-${Get.arguments['thru_date'].toString().substring(8, 10)}",
                    fontWeight: FontWeight.bold,
                    textAlign: TextAlign.right,
                  ),
                )
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Expanded(
                  child: MyText(
                    text: "Usia",
                  ),
                ),
                Expanded(
                  child: MyText(
                    text: Get.arguments['usia'].toString(),
                    fontWeight: FontWeight.bold,
                    textAlign: TextAlign.right,
                  ),
                )
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Expanded(
                  child: MyText(
                    text: "Total Premi",
                  ),
                ),
                Expanded(
                  child: MyText(
                    text: NumberFormat.decimalPattern()
                        .format(Get.arguments['total_premi'])
                        .toString(),
                    fontWeight: FontWeight.bold,
                    textAlign: TextAlign.right,
                  ),
                )
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Expanded(
                  child: MyText(
                    text: "Total Benefit",
                  ),
                ),
                Expanded(
                  child: MyText(
                    text: NumberFormat.decimalPattern()
                        .format(Get.arguments['total_benefit'])
                        .toString(),
                    fontWeight: FontWeight.bold,
                    textAlign: TextAlign.right,
                  ),
                )
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Expanded(
                  child: MyText(
                    text: "Status",
                  ),
                ),
                Expanded(
                  child: MyText(
                    text: Get.arguments['status'].toString(),
                    fontWeight: FontWeight.bold,
                    textAlign: TextAlign.right,
                  ),
                )
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Expanded(
                  child: MyText(
                    text: "Tgl Akseptasi",
                  ),
                ),
                Expanded(
                  child: MyText(
                    text: Get.arguments['acceptation_date']?.toString() ?? '-',
                    fontWeight: FontWeight.bold,
                    textAlign: TextAlign.right,
                  ),
                )
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Expanded(
                  child: MyText(
                    text: "No. Invoice",
                  ),
                ),
                Expanded(
                  child: MyText(
                    text: Get.arguments['no_invoice']?.toString() ?? '-',
                    fontWeight: FontWeight.bold,
                    textAlign: TextAlign.right,
                  ),
                )
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Expanded(
                  child: MyText(
                    text: "No. Refrensi",
                  ),
                ),
                Expanded(
                  child: MyText(
                    text: Get.arguments['no_referensi']?.toString() ?? '-',
                    fontWeight: FontWeight.bold,
                    textAlign: TextAlign.right,
                  ),
                )
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Expanded(
                  child: MyText(
                    text: "No. Batch",
                  ),
                ),
                Expanded(
                  child: MyText(
                    text: Get.arguments['no_batch']?.toString() ?? '-',
                    fontWeight: FontWeight.bold,
                    textAlign: TextAlign.right,
                  ),
                )
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Expanded(
                  child: MyText(
                    text: "STNC",
                  ),
                ),
                Expanded(
                  child: MyText(
                    text: Get.arguments['stnc_date']?.toString() ?? "-",
                    fontWeight: FontWeight.bold,
                    textAlign: TextAlign.right,
                  ),
                )
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Expanded(
                  child: MyText(
                    text: "Underwiriting status",
                  ),
                ),
                Expanded(
                  child: MyText(
                    text:
                        Get.arguments['underwriting_status']?.toString() ?? '',
                    fontWeight: FontWeight.bold,
                    textAlign: TextAlign.right,
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}

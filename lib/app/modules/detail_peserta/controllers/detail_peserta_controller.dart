// ignore_for_file: avoid_print

import 'package:amanahgitha_flutter/app/data/providers/sertifikat_provider.dart';
import 'package:amanahgitha_flutter/app/utils/colors.dart';
import 'package:amanahgitha_flutter/app/utils/snackbar.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:url_launcher/url_launcher.dart';

class DetailPesertaController extends GetxController {
  final isLoading = false.obs;
  final status = '0'.obs;

  getLinkSertifikat() async {
    try {
      isLoading.value = true;
      var params = {
        'pengguna': GetStorage().read('pengguna'),
        'policy_no': Get.arguments['policy_no'],
        'no_peserta': Get.arguments['no_peserta'],
      };
      print("params ${params.toString()}");
      Map<String, dynamic>? link =
          await SertifikatProvider().cetakSertifikat(params);
      if (link != null) {
        if (link['status']) {
          if (!await launch(link['link'])) throw 'Could not launch $link';
        } else {
          snackbar(
            title: "Link tidak ada",
            message: link['keterangan'].toString(),
            color: dangerBorderColor,
          );
        }
      } else {
        snackbar(
          title: "Info",
          message: "Response server null, coba kembali ...",
        );
      }
    } finally {
      isLoading.value = false;
    }
  }

  @override
  void onInit() {
    super.onInit();
    status.value = Get.arguments['status'].toString();
  }
}

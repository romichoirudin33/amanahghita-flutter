import 'package:amanahgitha_flutter/app/data/providers/auth_provider.dart';
import 'package:amanahgitha_flutter/app/routes/app_pages.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class LoginController extends GetxController {
  final isObscureText = true.obs;
  final errorText = ''.obs;
  final isLoading = false.obs;

  login(Map<String, dynamic> params) async {
    isLoading.value = true;
    var data = await AuthProvider().login(params);
    isLoading.value = false;
    if (data != null) {
      if (data['success'] && data['data']['keterangan'] == "login berhasil") {
        Get.offAllNamed(Routes.HOME);
        GetStorage().write('pengguna', params['pengguna']);
      } else {
        errorText.value = data['data']['keterangan'];
      }
    } else {
      errorText.value = 'Terjadi kesalahan, coba ulangi kembali';
    }
  }
}

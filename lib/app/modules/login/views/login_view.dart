// ignore_for_file: prefer_const_constructors, use_key_in_widget_constructors

import 'package:amanahgitha_flutter/app/utils/buttons.dart';
import 'package:amanahgitha_flutter/app/utils/colors.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/login_controller.dart';

class LoginView extends GetView<LoginController> {
  final TextEditingController usernameController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      body: Container(
        height: height,
        color: gray50,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Expanded(
              flex: 2,
              child: SizedBox(
                width: 150,
                child: Image.asset("assets/logo.png"),
              ),
            ),
            Expanded(
              flex: 3,
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30.0),
                    topRight: Radius.circular(30.0),
                  ),
                  color: whiteColor,
                ),
                padding: EdgeInsets.symmetric(horizontal: 25),
                child: Column(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(top: 50, bottom: 10),
                      child: TextField(
                        onChanged: (text) {},
                        controller: usernameController,
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.symmetric(horizontal: 12),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(6),
                          ),
                          hintText: 'Username',
                          floatingLabelBehavior: FloatingLabelBehavior.always,
                          suffixIcon: Icon(Icons.person),
                        ),
                      ),
                    ),
                    Obx(
                      () => Padding(
                        padding: EdgeInsets.only(bottom: 47),
                        child: TextField(
                          onChanged: (text) {},
                          obscureText: controller.isObscureText.value,
                          controller: passwordController,
                          decoration: InputDecoration(
                            contentPadding:
                                EdgeInsets.symmetric(horizontal: 12),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(6),
                            ),
                            hintText: 'Password',
                            errorText: controller.errorText.value != ''
                                ? controller.errorText.value
                                : null,
                            floatingLabelBehavior: FloatingLabelBehavior.always,
                            suffixIcon: IconButton(
                              icon: Icon(controller.isObscureText.value
                                  ? Icons.visibility_off
                                  : Icons.visibility),
                              onPressed: () {
                                controller.isObscureText.value =
                                    !controller.isObscureText.value;
                              },
                            ),
                          ),
                        ),
                      ),
                    ),
                    Obx(
                      () => controller.isLoading.value
                          ? SizedBox(
                              height: 45,
                              width: 45,
                              child: CircularProgressIndicator(),
                            )
                          : SizedBox(
                              height: 48,
                              width: width,
                              child: MyButton(
                                text: 'Login',
                                onPressed: () {
                                  // usernameController.text = 'adminbandung';
                                  // passwordController.text = 'admin';

                                  controller.errorText.value = '';
                                  if (usernameController.text.isEmpty ||
                                      passwordController.text.isEmpty) {
                                    controller.errorText.value =
                                        'Username dan password tidak boleh kosong';
                                  } else {
                                    controller.login({
                                      'pengguna': usernameController.text,
                                      'katakunci': passwordController.text,
                                    });
                                  }
                                },
                              ),
                            ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

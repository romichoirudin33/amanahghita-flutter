import 'package:get/get.dart';

import '../controllers/detail_peserta_batal_controller.dart';

class DetailPesertaBatalBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DetailPesertaBatalController>(
      () => DetailPesertaBatalController(),
    );
  }
}

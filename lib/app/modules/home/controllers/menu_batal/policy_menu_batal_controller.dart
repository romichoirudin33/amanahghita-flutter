// ignore_for_file: invalid_use_of_protected_member

import 'package:amanahgitha_flutter/app/data/models/policy_menu_batal.dart';
import 'package:amanahgitha_flutter/app/data/models/filter_pencarian.dart';
import 'package:amanahgitha_flutter/app/data/providers/menu_batal/policy_provider.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class PolicyMenuBatalController extends GetxController {
  final policyList = <Policy>[].obs;
  final policySelected = "".obs;
  final isLoadingPolicy = false.obs;

  final filterPolicy = <FilterPencarian>[].obs;
  final filterPolicyList = <String>[].obs;
  final filterPolicySelected = 'No Polis'.obs;

  final errorText = ''.obs;

  getFilterPolicy(Map<String, dynamic> params) async {
    var data = await PolicyProvider().getFilterPolicy(params);
    if (data != null) {
      data = data;
    } else {
      data = [
        FilterPencarian(
          label: 'No Polis',
          value: 'policy_no',
        )
      ];
    }

    filterPolicy.value = data;

    filterPolicyList.value.clear();
    for (FilterPencarian element in data) {
      filterPolicyList.value.add(element.label.toString());
    }
    filterPolicySelected.value = filterPolicyList.value[0];
  }

  cariPolicy(String key) async {
    try {
      String field = '';
      for (FilterPencarian element in filterPolicy.value) {
        if (element.label == filterPolicySelected.value) {
          field = element.value.toString();
        }
      }
      var params = {
        'pengguna': GetStorage().read('pengguna'),
        'field': field,
        'value': key,
      };
      isLoadingPolicy.value = true;

      var data = await PolicyProvider().getListPolicy(params);
      if (data != null) {
        policyList.value = data;
      }
    } finally {
      isLoadingPolicy.value = false;
    }
  }

  reset() {
    policyList.value = [];
    policySelected.value = '';
    filterPolicySelected.value = filterPolicyList.value[0];
  }

  @override
  void onInit() async {
    super.onInit();
    await getFilterPolicy({'pengguna': GetStorage().read('pengguna')});
  }
}

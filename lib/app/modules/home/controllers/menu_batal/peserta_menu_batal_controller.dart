// ignore_for_file: invalid_use_of_protected_member

import 'package:amanahgitha_flutter/app/data/models/action_member.dart';
import 'package:amanahgitha_flutter/app/data/models/filter_pencarian.dart';
import 'package:amanahgitha_flutter/app/data/models/peserta_menu_batal.dart';
import 'package:amanahgitha_flutter/app/data/providers/menu_batal/peserta_provider.dart';
import 'package:amanahgitha_flutter/app/modules/home/controllers/menu_batal/policy_menu_batal_controller.dart';
import 'package:amanahgitha_flutter/app/utils/colors.dart';
import 'package:amanahgitha_flutter/app/utils/snackbar.dart';
import 'package:flutter/foundation.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class PesertaMenuBatalController extends GetxController {
  final PolicyMenuBatalController controller = Get.find();

  final pesertaList = <Peserta>[].obs;
  final isLoadingPeserta = false.obs;

  final filterPeserta = <FilterPencarian>[].obs;
  final filterPesertaList = <String>[].obs;

  final filter = <FilterPencarian>[].obs;
  final filterList = <String>[].obs;

  final filterPesertaSelected = ''.obs;
  final keySelected = ''.obs;
  final filterSelected = ''.obs;
  final limitSeleted = 25.obs;
  final offsetSeleted = 0.obs;

  final totalPage = 1.obs;
  final pageSelected = 1.obs;

  final errorText = ''.obs;

  getFilterPeserta(Map<String, dynamic> params) async {
    var data = await PesertaProvider().getFilterPeserta(params);
    if (data != null) {
      data = data;
    } else {
      data = [
        FilterPencarian(
          label: 'No Polis',
          value: 'policy_no',
        )
      ];
    }

    filterPeserta.value = data;

    filterPesertaList.value.clear();
    for (FilterPencarian element in data) {
      filterPesertaList.value.add(element.label.toString());
    }
    filterPesertaSelected.value = filterPesertaList.value[0];
  }

  getFilter(Map<String, dynamic> params) async {
    var data = await PesertaProvider().getFilter(params);
    if (data != null) {
      data = data;
    } else {
      data = [
        FilterPencarian(
          label: 'All',
          value: '4',
        )
      ];
    }

    filter.value = data;

    filterList.value.clear();
    for (FilterPencarian element in data) {
      filterList.value.add(element.label.toString());
    }
    filterSelected.value = filterList.value[0];
  }

  cariPeserta() async {
    try {
      isLoadingPeserta.value = true;
      String field = '';
      for (FilterPencarian element in filterPeserta.value) {
        if (element.label == filterPesertaSelected.value) {
          field = element.value.toString();
        }
      }

      String filterLabel = '';
      for (FilterPencarian element in filter.value) {
        if (element.label == filterSelected.value) {
          filterLabel = element.value.toString();
        }
      }
      var params = {
        'pengguna': GetStorage().read('pengguna'),
        'policy_no': controller.policySelected.value,
        'field': field,
        'value': keySelected.value,
        'filter': filterLabel,
        'limit': limitSeleted,
        'offset': offsetSeleted,
      };
      if (kDebugMode) {
        print(params.toString());
      }

      PesertaMenuBatal? res = await PesertaProvider().getPeserta(params);
      if (res != null) {
        totalPage.value = res.page ?? 1;
        pesertaList.value = res.data ?? [];
      }
    } finally {
      isLoadingPeserta.value = false;
    }
  }

  hapusPeserta(Map params) async {
    try {
      if (kDebugMode) {
        print(params.toString());
      }
      ActionMember? res = await PesertaProvider().deleteMember(params);
      if (res != null) {
        if (res.success!) {
          if (res.data!.status == true) {
            snackbar(
              title: 'Info',
              message: res.data!.keterangan ?? '',
            );
          } else {
            snackbar(
              title: 'Info',
              message: res.data!.keterangan ?? '',
              color: dangerColor,
            );
          }
        } else {
          snackbar(
            title: 'Info',
            message: 'Aksi gagal',
            color: dangerColor,
          );
        }
      } else {
        snackbar(
          title: 'Info',
          message: 'Terjadi kesalahan, coba lagi',
          color: dangerColor,
        );
      }
    } finally {
      cariPeserta();
    }
  }

  reset() {
    pesertaList.value = [];
    filterPesertaSelected.value = filterPesertaList.value[0];
    keySelected.value = '';
    filterSelected.value = filterList.value[0];
    limitSeleted.value = 25;
    offsetSeleted.value = 0;
    totalPage.value = 1;
    pageSelected.value = 1;
  }

  @override
  void onInit() async {
    super.onInit();
    await getFilterPeserta({'pengguna': GetStorage().read('pengguna')});
    await getFilter({'pengguna': GetStorage().read('pengguna')});
  }
}

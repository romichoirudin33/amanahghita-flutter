import 'package:amanahgitha_flutter/app/modules/home/views/menu_batal_view.dart';
import 'package:amanahgitha_flutter/app/modules/home/views/menu_entry_view.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HomeController extends GetxController {
  final items = RxList<Widget>([
    const MenuEntryView(),
    const MenuBatalView(),
  ]);
  final index = RxInt(0);
}

// ignore_for_file: invalid_use_of_protected_member

import 'package:amanahgitha_flutter/app/data/models/Peserta.dart';
import 'package:amanahgitha_flutter/app/data/models/Policy.dart';
import 'package:get/get.dart';

class MenuBatalController extends GetxController {
  final policyList = <Policy>[].obs;
  final policySelected = "0".obs;
  final isLoadingPolicy = false.obs;

  final pesertaList = <Peserta>[].obs;
  final isLoadingPeserta = false.obs;

  final pesertaCancelList = <Peserta>[].obs;
  final isLoadingPesertaCancel = false.obs;

  void cariPolicy() {
    isLoadingPolicy.value = true;
    isLoadingPeserta.value = true;

    Future.delayed(const Duration(seconds: 2), () {
      isLoadingPolicy.value = false;
      isLoadingPeserta.value = false;
      pesertaList.value.clear();

      policySelected.value = "0";

      for (var i = 1; i < 6; i++) {
        policyList.value.add(
          Policy(
            policyHolder: "holder",
            policyNo: 'policy no ${i.toString()}',
            productId: i.toString(),
            productName: 'product name',
            spaNoGroup: 'spa no group',
          ),
        );
      }
    });
  }

  void cariPeserta() {
    isLoadingPeserta.value = true;

    Future.delayed(const Duration(seconds: 2), () {
      isLoadingPeserta.value = false;

      for (var i = 0; i < 6; i++) {
        pesertaList.value.add(
          Peserta(
            policyNo: "policy no",
            noPeserta: "no peserta",
            nama: "nama",
            tanggalLahir: "tanggal lahir",
            fromDate: "from date",
            thruDate: "thrudate",
            status: "status",
            usia: 34,
            totalPremi: "123",
            totalBenefit: "123",
            extraRate: "123",
            rate: "123",
            productId: "123",
            weight: "123",
            height: "123",
            acceptationDate: "",
            noInvoice: null,
            noReferensi: null,
            noBatch: null,
            stncDate: null,
          ),
        );
      }
    });
  }

  void cariPesertaCancel() {
    isLoadingPesertaCancel.value = true;

    Future.delayed(const Duration(seconds: 2), () {
      isLoadingPesertaCancel.value = false;

      for (var i = 0; i < 6; i++) {
        pesertaCancelList.value.add(
          Peserta(
            policyNo: "policy no",
            noPeserta: "no peserta",
            nama: "nama",
            tanggalLahir: "tanggal lahir",
            fromDate: "from date",
            thruDate: "thrudate",
            status: "status",
            usia: 34,
            totalPremi: "123",
            totalBenefit: "123",
            extraRate: "123",
            rate: "123",
            productId: "123",
            weight: "123",
            height: "123",
            acceptationDate: "",
            noInvoice: null,
            noReferensi: null,
            noBatch: null,
            stncDate: null,
          ),
        );
      }
    });
  }
}

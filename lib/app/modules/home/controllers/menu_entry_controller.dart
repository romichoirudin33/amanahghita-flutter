// ignore_for_file: invalid_use_of_protected_member

import 'package:amanahgitha_flutter/app/data/models/list_peserta.dart';
import 'package:amanahgitha_flutter/app/data/models/list_policy.dart';
import 'package:amanahgitha_flutter/app/data/providers/menu_entry/peserta_provider.dart';
import 'package:amanahgitha_flutter/app/data/providers/menu_entry/policy_provider.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class MenuEntryController extends GetxController {
  final policyList = <Policy>[].obs;
  final policySelected = "".obs;
  final isLoadingPolicy = false.obs;
  final spaNoGroupSelected = "".obs;
  final productNameSelected = "".obs;

  final pesertaList = <Peserta>[].obs;
  final isLoadingPeserta = false.obs;

  final filterPolicy = {}.obs;
  final filterPolicyList = <String>[].obs;
  final filterPolicySelected = 'Policy No'.obs;

  final filterPeserta = {}.obs;
  final filterPesertaList = <String>[].obs;
  final filterPesertaSelected = 'Policy No'.obs;

  final errorText = ''.obs;

  getFilterPolicy(Map<String, dynamic> params) async {
    Map<String, dynamic>? data = await PolicyProvider().getFilterPolicy(params);
    if (data != null) {
      data = data;
    } else {
      data = {'policy_no': 'Policy No'};
    }

    filterPolicy.value = data;

    filterPolicyList.value.clear();
    data.forEach((key, value) => filterPolicyList.value.add(value));
    filterPolicySelected.value = filterPolicyList.value[0];
  }

  getFilterPeserta(Map<String, dynamic> params) async {
    Map<String, dynamic>? data =
        await PesertaProvider().getFilterPeserta(params);
    if (data != null) {
      data = data;
    } else {
      data = {"nama": "Nama Peserta"};
    }

    filterPeserta.value = data;

    filterPesertaList.value.clear();
    data.forEach((key, value) => filterPesertaList.value.add(value));
    filterPesertaSelected.value = filterPesertaList.value[0];
  }

  cariPolicy(String key) async {
    try {
      String field = filterPolicy.value.keys.firstWhere(
        (k) => filterPolicy.value[k] == filterPolicySelected.value,
        orElse: () => '',
      );
      var params = {
        'pengguna': GetStorage().read('pengguna'),
        'field': field,
        'filter': key,
      };
      isLoadingPolicy.value = true;
      isLoadingPeserta.value = true;

      var data = await PolicyProvider().getListPolicy(params);
      if (data != null) {
        policyList.value = data;
      }
    } finally {
      isLoadingPolicy.value = false;
      isLoadingPeserta.value = false;
      pesertaList.value.clear();
    }
  }

  cariPeserta(String key) async {
    try {
      isLoadingPeserta.value = true;
      String field = filterPeserta.value.keys.firstWhere(
        (k) => filterPeserta.value[k] == filterPesertaSelected.value,
        orElse: () => '',
      );
      var params = {
        'pengguna': GetStorage().read('pengguna'),
        'policy_no': policySelected,
        'field': field,
        'filter': key,
      };

      var data = await PesertaProvider().getListPeserta(params);
      if (data != null) {
        pesertaList.value = data;
      }
    } finally {
      isLoadingPeserta.value = false;
    }
  }

  reset() {
    policyList.value = [];
    policySelected.value = '';
    filterPolicySelected.value = filterPolicyList.value[0];
    pesertaList.value = [];
    filterPesertaSelected.value = filterPesertaList.value[0];
  }

  @override
  void onInit() async {
    super.onInit();
    await getFilterPolicy({'pengguna': GetStorage().read('pengguna')});
    await getFilterPeserta({'pengguna': GetStorage().read('pengguna')});
  }
}

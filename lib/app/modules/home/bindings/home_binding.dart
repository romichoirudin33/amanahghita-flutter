import 'package:get/get.dart';

import 'package:amanahgitha_flutter/app/modules/home/controllers/menu_batal_controller.dart';
import 'package:amanahgitha_flutter/app/modules/home/controllers/menu_entry_controller.dart';
import 'package:amanahgitha_flutter/app/modules/home/controllers/menu_batal/peserta_cancel_menu_batal_controller.dart';
import 'package:amanahgitha_flutter/app/modules/home/controllers/menu_batal/peserta_menu_batal_controller.dart';
import 'package:amanahgitha_flutter/app/modules/home/controllers/menu_batal/policy_menu_batal_controller.dart';

import '../controllers/home_controller.dart';

class HomeBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PesertaCancelMenuBatalController>(
      () => PesertaCancelMenuBatalController(),
    );
    Get.lazyPut<PesertaMenuBatalController>(
      () => PesertaMenuBatalController(),
    );
    Get.lazyPut<PolicyMenuBatalController>(
      () => PolicyMenuBatalController(),
    );
    Get.lazyPut<MenuBatalController>(
      () => MenuBatalController(),
    );
    Get.lazyPut<MenuEntryController>(
      () => MenuEntryController(),
    );
    Get.lazyPut<HomeController>(
      () => HomeController(),
    );
  }
}

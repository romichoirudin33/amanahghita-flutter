import 'package:amanahgitha_flutter/app/modules/home/controllers/menu_entry_controller.dart';
import 'package:amanahgitha_flutter/app/modules/home/views/menu_entry/peserta/peserta_state.dart';
import 'package:amanahgitha_flutter/app/modules/home/views/menu_entry/policy/policy_state.dart';
import 'package:amanahgitha_flutter/app/routes/app_pages.dart';
import 'package:amanahgitha_flutter/app/utils/colors.dart';
import 'package:amanahgitha_flutter/app/utils/snackbar.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

class MenuEntryView extends GetView<MenuEntryController> {
  const MenuEntryView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 8),
            child: Column(
              children: const [
                Expanded(
                  flex: 1,
                  child: PolicyState(),
                ),
                Expanded(
                  flex: 1,
                  child: PesertaState(),
                ),
              ],
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          if (controller.policySelected.value == '' ||
              controller.spaNoGroupSelected.value == '') {
            snackbar(
              title: "Info",
              message: "Tidak terdapat policy yang di pilih",
              color: dangerBorderColor,
            );
          } else {
            Get.toNamed(
              Routes.TAMBAH_ENTRY,
              arguments: {
                'no_policy': controller.policySelected,
                'spa_no_group': controller.spaNoGroupSelected,
                'product_name': controller.productNameSelected,
              },
            );
          }
        },
        backgroundColor: mainColor,
        child: const Icon(Icons.add),
      ),
    );
  }
}

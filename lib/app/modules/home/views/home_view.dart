// ignore_for_file: prefer_const_constructors, use_key_in_widget_constructors

import 'package:amanahgitha_flutter/app/modules/home/controllers/menu_batal/peserta_cancel_menu_batal_controller.dart';
import 'package:amanahgitha_flutter/app/modules/home/controllers/menu_batal/peserta_menu_batal_controller.dart';
import 'package:amanahgitha_flutter/app/modules/home/controllers/menu_batal/policy_menu_batal_controller.dart';
import 'package:amanahgitha_flutter/app/modules/home/controllers/menu_entry_controller.dart';
import 'package:amanahgitha_flutter/app/routes/app_pages.dart';
import 'package:amanahgitha_flutter/app/utils/app_bar.dart';
import 'package:amanahgitha_flutter/app/utils/colors.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  final MenuEntryController menuEntryController = Get.find();
  final PolicyMenuBatalController policyMenuBatalController = Get.find();
  final PesertaMenuBatalController pesertaMenuBatalController = Get.find();
  final PesertaCancelMenuBatalController pesertaCancelMenuBatalController =
      Get.find();

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Scaffold(
        appBar: MyAppBar(
          text: 'Amanah Ghita',
          actions: [
            IconButton(
              onPressed: () {
                menuEntryController.reset();
                policyMenuBatalController.reset();
                pesertaMenuBatalController.reset();
                pesertaCancelMenuBatalController.reset();
              },
              icon: Icon(Icons.refresh),
            ),
            IconButton(
              onPressed: () {
                Get.dialog(
                  AlertDialog(
                    title: const Text('Logout'),
                    content: const Text('Anda yakin akan logout ?'),
                    actions: [
                      TextButton(
                        child: const Text("Tidak"),
                        onPressed: () => Get.back(),
                      ),
                      TextButton(
                        child: const Text("Ya"),
                        onPressed: () {
                          GetStorage().remove('pengguna');
                          Get.offAllNamed(Routes.LOGIN);
                        },
                      ),
                    ],
                  ),
                );
              },
              icon: Icon(Icons.power_settings_new_rounded),
            ),
          ],
        ),
        body: controller.items[controller.index.value],
        bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          items: const [
            BottomNavigationBarItem(
              icon: Icon(Icons.list),
              label: 'Menu Entry',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.announcement_outlined),
              label: 'Menu Batal',
            ),
          ],
          onTap: (int index) => controller.index.value = index,
          currentIndex: controller.index.value,
          selectedItemColor: logoColor,
        ),
      ),
    );
  }
}

// ignore_for_file: must_be_immutable

import 'package:amanahgitha_flutter/app/data/models/list_policy.dart';
import 'package:amanahgitha_flutter/app/modules/home/controllers/menu_entry_controller.dart';
import 'package:amanahgitha_flutter/app/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ListPolicy extends GetView<MenuEntryController> {
  const ListPolicy({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      separatorBuilder: (context, index) => const SizedBox(
        height: 10,
      ),
      scrollDirection: Axis.vertical,
      itemCount: controller.policyList.length,
      itemBuilder: (BuildContext ctx, int index) {
        return InkWell(
          onTap: () {
            if (controller.policyList[index].productId != null) {
              controller.policySelected.value =
                  controller.policyList[index].policyNo!;
              controller.spaNoGroupSelected.value =
                  controller.policyList[index].spaNoGroup!;
              controller.productNameSelected.value =
                  controller.policyList[index].productName!;
            }
          },
          child: ItemPolicy(
            item: controller.policyList[index],
          ),
        );
      },
    );
  }
}

class ItemPolicy extends StatelessWidget {
  ItemPolicy({Key? key, required this.item}) : super(key: key);

  final MenuEntryController controller = Get.find();
  final Policy item;

  Color selectedColor = green400;
  Color unSelectedColor = gray;

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Container(
        width: Get.mediaQuery.size.width,
        height: 56.0,
        padding: const EdgeInsets.symmetric(horizontal: 14.0),
        decoration: BoxDecoration(
          color: whiteColor,
          border: Border(
            left: BorderSide(
              color: controller.policySelected.value == item.policyNo
                  ? selectedColor
                  : unSelectedColor,
              width: 3.0,
            ),
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    item.policyNo ?? '',
                    style: TextStyle(color: gray, fontSize: 10),
                  ),
                  Text(
                    item.productName ?? '',
                    style: TextStyle(
                        color: controller.policySelected.value == item.policyNo
                            ? selectedColor
                            : unSelectedColor,
                        fontSize: 12),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Text(
                    item.spaNoGroup ?? '',
                    style: TextStyle(color: gray, fontSize: 10),
                  ),
                  Text(
                    item.policyHolder ?? '',
                    textAlign: TextAlign.end,
                    style: TextStyle(color: gray500, fontSize: 12),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

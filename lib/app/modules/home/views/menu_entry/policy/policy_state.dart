import 'package:amanahgitha_flutter/app/modules/home/controllers/menu_entry_controller.dart';
import 'package:amanahgitha_flutter/app/modules/home/views/menu_entry/list_loading.dart';
import 'package:amanahgitha_flutter/app/modules/home/views/menu_entry/policy/list_policy.dart';
import 'package:amanahgitha_flutter/app/utils/buttons.dart';
import 'package:amanahgitha_flutter/app/utils/colors.dart';
import 'package:amanahgitha_flutter/app/utils/empty_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class PolicyState extends GetView<MenuEntryController> {
  const PolicyState({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final TextEditingController keyEditingController = TextEditingController();

    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const Text('Daftar Polis'),
            IconButton(
              onPressed: () {
                controller.errorText.value = '';
                Get.bottomSheet(
                  Container(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 20,
                      vertical: 20,
                    ),
                    child: Wrap(
                      children: [
                        Center(
                          child: Container(
                            margin: const EdgeInsets.only(bottom: 15),
                            height: 4,
                            width: 80,
                            color: Colors.black54,
                          ),
                        ),
                        const Padding(
                          padding: EdgeInsets.only(bottom: 10),
                          child: Text(
                            'Pencarian Policy',
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 15.0),
                          child: InputDecorator(
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: whiteColor,
                              contentPadding: const EdgeInsets.symmetric(
                                horizontal: 12,
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: gray500),
                                borderRadius: BorderRadius.circular(6.0),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: gray100),
                                borderRadius: BorderRadius.circular(6.0),
                              ),
                              hintText: 'Status Absen',
                            ),
                            child: DropdownButtonHideUnderline(
                              child: Obx(
                                () => DropdownButton<String>(
                                  value: controller.filterPolicySelected.value,
                                  icon: const Icon(Icons.arrow_drop_down),
                                  style: TextStyle(
                                    fontSize: 12.0,
                                    color: gray500,
                                  ),
                                  onChanged: (String? newValue) {
                                    if (newValue != null) {
                                      controller.filterPolicySelected.value =
                                          newValue;
                                    }
                                  },
                                  items: controller.filterPolicyList
                                      .map<DropdownMenuItem<String>>(
                                          (String value) {
                                    return DropdownMenuItem<String>(
                                      value: value,
                                      child: Text(value),
                                    );
                                  }).toList(),
                                ),
                              ),
                            ),
                          ),
                        ),
                        Obx(
                          () => TextField(
                            style: TextStyle(
                              fontSize: 12.0,
                              color: gray500,
                            ),
                            onChanged: (text) {},
                            controller: keyEditingController,
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: whiteColor,
                              errorText: controller.errorText.value != ''
                                  ? controller.errorText.value
                                  : null,
                              contentPadding:
                                  const EdgeInsets.symmetric(horizontal: 12),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: gray500),
                                borderRadius: BorderRadius.circular(6.0),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: gray100),
                                borderRadius: BorderRadius.circular(6.0),
                              ),
                              hintText: 'Masukkan key',
                              hintStyle: TextStyle(
                                fontSize: 12.0,
                                color: gray400,
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 20),
                          child: SizedBox(
                            width: Get.mediaQuery.size.width,
                            height: 40,
                            child: MyButton(
                              text: 'Cari',
                              onPressed: () {
                                controller.errorText.value = '';
                                controller.cariPolicy(
                                  keyEditingController.text.toString(),
                                );
                                Get.back();
                              },
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  backgroundColor: whiteColor,
                  isDismissible: true,
                  enableDrag: true,
                  shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20),
                      topRight: Radius.circular(20),
                    ),
                  ),
                );
              },
              icon: const Icon(Icons.filter_list),
            ),
          ],
        ),
        Obx(
          () => controller.isLoadingPolicy.value
              ? const Expanded(
                  child: ListLoading(),
                )
              : controller.policyList.isNotEmpty
                  ? const Expanded(
                      child: ListPolicy(),
                    )
                  : const EmptyWidget(msg: 'Tidak terdapat data'),
        ),
      ],
    );
  }
}

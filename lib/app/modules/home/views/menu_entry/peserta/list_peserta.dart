// ignore_for_file: must_be_immutable

import 'package:amanahgitha_flutter/app/data/models/list_peserta.dart';
import 'package:amanahgitha_flutter/app/modules/home/controllers/menu_entry_controller.dart';
import 'package:amanahgitha_flutter/app/routes/app_pages.dart';
import 'package:amanahgitha_flutter/app/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ListPeserta extends StatelessWidget {
  ListPeserta({Key? key}) : super(key: key);

  final MenuEntryController controller = Get.find();

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      separatorBuilder: (context, index) => const SizedBox(
        height: 10,
      ),
      scrollDirection: Axis.vertical,
      itemCount: controller.pesertaList.length,
      itemBuilder: (BuildContext ctx, int index) {
        return InkWell(
          onTap: () {
            Get.toNamed(
              Routes.DETAIL_PESERTA,
              arguments: controller.pesertaList[index].toJson(),
            );
          },
          child: ItemPeserta(
            item: controller.pesertaList[index],
          ),
        );
      },
    );
  }
}

class ItemPeserta extends StatelessWidget {
  ItemPeserta({Key? key, required this.item}) : super(key: key);

  final Peserta item;

  Color selectedColor = green400;
  Color unSelectedColor = gray;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.mediaQuery.size.width,
      height: 56.0,
      padding: const EdgeInsets.symmetric(horizontal: 14.0),
      decoration: BoxDecoration(
        color: whiteColor,
        border: Border(
          left: BorderSide(
            color: unSelectedColor,
            width: 3.0,
          ),
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  item.noPeserta ?? '',
                  style: TextStyle(color: gray, fontSize: 10),
                ),
                Text(
                  item.nama ?? '-',
                  style: TextStyle(color: unSelectedColor, fontSize: 12),
                ),
              ],
            ),
          ),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Text(
                  "Tgl Lahir : ${item.tanggalLahir!.year.toString().padLeft(4, '0')}-${item.tanggalLahir!.month.toString().padLeft(2, '0')}-${item.tanggalLahir!.day.toString().padLeft(2, '0')}",
                  style: TextStyle(color: gray, fontSize: 10),
                ),
                Text(
                  "Awal Kontrak : ${item.fromDate!.year.toString().padLeft(4, '0')}-${item.fromDate!.month.toString().padLeft(2, '0')}-${item.fromDate!.day.toString().padLeft(2, '0')}",
                  style: TextStyle(color: gray500, fontSize: 12),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

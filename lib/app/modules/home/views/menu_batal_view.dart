import 'package:amanahgitha_flutter/app/modules/home/views/menu_batal/peserta/peserta_state.dart';
import 'package:amanahgitha_flutter/app/modules/home/views/menu_batal/peserta_cancel/peserta_cancel_state.dart';
import 'package:amanahgitha_flutter/app/modules/home/views/menu_batal/policy/policy_state.dart';
import 'package:amanahgitha_flutter/app/utils/colors.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

class MenuBatalView extends GetView {
  const MenuBatalView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 15),
          child: Column(
            children: [
              Container(
                height: 350,
                padding: const EdgeInsets.symmetric(horizontal: 15),
                margin: const EdgeInsets.only(bottom: 20),
                decoration: BoxDecoration(
                  borderRadius: const BorderRadius.all(
                    Radius.circular(10.0),
                  ),
                  boxShadow: const [
                    BoxShadow(
                      color: Colors.grey,
                      blurRadius: 1.0,
                    ),
                  ],
                  color: whiteColor,
                ),
                child: const PolicyState(),
              ),
              Container(
                height: 450,
                padding: const EdgeInsets.symmetric(horizontal: 15),
                margin: const EdgeInsets.only(bottom: 20),
                decoration: BoxDecoration(
                  borderRadius: const BorderRadius.all(
                    Radius.circular(10.0),
                  ),
                  boxShadow: const [
                    BoxShadow(
                      color: Colors.grey,
                      blurRadius: 1.0,
                    ),
                  ],
                  color: whiteColor,
                ),
                child: const PesertaState(),
              ),
              Container(
                height: 450,
                padding: const EdgeInsets.symmetric(horizontal: 15),
                margin: const EdgeInsets.only(bottom: 20),
                decoration: BoxDecoration(
                  borderRadius: const BorderRadius.all(
                    Radius.circular(10.0),
                  ),
                  boxShadow: const [
                    BoxShadow(
                      color: Colors.grey,
                      blurRadius: 1.0,
                    ),
                  ],
                  color: whiteColor,
                ),
                child: const PesertaCancelState(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

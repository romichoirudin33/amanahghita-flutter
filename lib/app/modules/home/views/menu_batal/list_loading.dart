import 'package:amanahgitha_flutter/app/utils/colors.dart';
import 'package:amanahgitha_flutter/app/utils/loading.dart';
import 'package:flutter/material.dart';

class ListLoading extends StatelessWidget {
  const ListLoading({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      scrollDirection: Axis.vertical,
      itemCount: 5,
      itemBuilder: (BuildContext ctx, int index) {
        return const ItemLoading();
      },
    );
  }
}

class ItemLoading extends StatelessWidget {
  const ItemLoading({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return Container(
      width: width,
      height: 56.0,
      margin: const EdgeInsets.only(bottom: 10.0),
      padding: const EdgeInsets.symmetric(horizontal: 14.0),
      decoration: BoxDecoration(
        color: whiteColor,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              LoadingShimmer(
                width: width / 3,
                height: 10,
                margin: const EdgeInsets.only(bottom: 4.0),
              ),
              LoadingShimmer(width: width / 3, height: 10),
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              LoadingShimmer(
                  width: width / 3,
                  height: 10,
                  margin: const EdgeInsets.only(bottom: 4.0)),
              LoadingShimmer(width: width / 3, height: 10),
            ],
          ),
        ],
      ),
    );
  }
}

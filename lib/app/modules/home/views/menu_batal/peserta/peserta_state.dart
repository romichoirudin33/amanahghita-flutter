import 'package:amanahgitha_flutter/app/modules/home/controllers/menu_batal/peserta_menu_batal_controller.dart';
import 'package:amanahgitha_flutter/app/modules/home/views/menu_batal/list_loading.dart';
import 'package:amanahgitha_flutter/app/modules/home/views/menu_batal/peserta/list_peserta.dart';
import 'package:amanahgitha_flutter/app/utils/buttons.dart';
import 'package:amanahgitha_flutter/app/utils/colors.dart';
import 'package:amanahgitha_flutter/app/utils/empty_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class PesertaState extends GetView<PesertaMenuBatalController> {
  const PesertaState({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final TextEditingController keyEditingController = TextEditingController();

    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const Text('Daftar Peserta'),
            IconButton(
              onPressed: () {
                Get.bottomSheet(
                  Container(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 20,
                      vertical: 20,
                    ),
                    child: ListView(
                      children: [
                        Center(
                          child: Container(
                            margin: const EdgeInsets.only(bottom: 15),
                            height: 4,
                            width: 80,
                            color: Colors.black54,
                          ),
                        ),
                        const Padding(
                          padding: EdgeInsets.only(bottom: 10),
                          child: Text(
                            'Pencarian Peserta Berdasarkan',
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 15.0),
                          child: InputDecorator(
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: whiteColor,
                              contentPadding: const EdgeInsets.symmetric(
                                horizontal: 12,
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: gray500),
                                borderRadius: BorderRadius.circular(6.0),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: gray100),
                                borderRadius: BorderRadius.circular(6.0),
                              ),
                              hintText: '',
                            ),
                            child: DropdownButtonHideUnderline(
                              child: Obx(
                                () => DropdownButton<String>(
                                  value: controller.filterPesertaSelected.value,
                                  icon: const Icon(Icons.arrow_drop_down),
                                  style: TextStyle(
                                    fontSize: 12.0,
                                    color: gray500,
                                  ),
                                  onChanged: (String? newValue) {
                                    if (newValue != null) {
                                      controller.filterPesertaSelected.value =
                                          newValue;
                                    }
                                  },
                                  items: controller.filterPesertaList
                                      .map<DropdownMenuItem<String>>(
                                          (String value) {
                                    return DropdownMenuItem<String>(
                                      value: value,
                                      child: Text(value),
                                    );
                                  }).toList(),
                                ),
                              ),
                            ),
                          ),
                        ),
                        const Padding(
                          padding: EdgeInsets.only(bottom: 10),
                          child: Text(
                            'Masukkan pencarian',
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 15.0),
                          child: TextField(
                            style: TextStyle(
                              fontSize: 12.0,
                              color: gray500,
                            ),
                            onChanged: (text) {
                              controller.keySelected.value = text.toString();
                            },
                            controller: keyEditingController,
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: whiteColor,
                              contentPadding:
                                  const EdgeInsets.symmetric(horizontal: 12),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: gray500),
                                borderRadius: BorderRadius.circular(6.0),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: gray100),
                                borderRadius: BorderRadius.circular(6.0),
                              ),
                              hintText: 'Masukkan key',
                              hintStyle: TextStyle(
                                fontSize: 12.0,
                                color: gray400,
                              ),
                            ),
                          ),
                        ),
                        const Padding(
                          padding: EdgeInsets.only(bottom: 10),
                          child: Text(
                            'Filter Berdasarkan',
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 15.0),
                          child: InputDecorator(
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: whiteColor,
                              contentPadding: const EdgeInsets.symmetric(
                                horizontal: 12,
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: gray500),
                                borderRadius: BorderRadius.circular(6.0),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: gray100),
                                borderRadius: BorderRadius.circular(6.0),
                              ),
                              hintText: '',
                            ),
                            child: DropdownButtonHideUnderline(
                              child: Obx(
                                () => DropdownButton<String>(
                                  value: controller.filterSelected.value,
                                  icon: const Icon(Icons.arrow_drop_down),
                                  style: TextStyle(
                                    fontSize: 12.0,
                                    color: gray500,
                                  ),
                                  onChanged: (String? newValue) {
                                    if (newValue != null) {
                                      controller.filterSelected.value =
                                          newValue;
                                    }
                                  },
                                  items: controller.filterList
                                      .map<DropdownMenuItem<String>>(
                                          (String value) {
                                    return DropdownMenuItem<String>(
                                      value: value,
                                      child: Text(value),
                                    );
                                  }).toList(),
                                ),
                              ),
                            ),
                          ),
                        ),
                        const Padding(
                          padding: EdgeInsets.only(bottom: 10),
                          child: Text(
                            'Tampilkan',
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 15.0),
                          child: InputDecorator(
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: whiteColor,
                              contentPadding: const EdgeInsets.symmetric(
                                horizontal: 12,
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: gray500),
                                borderRadius: BorderRadius.circular(6.0),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: gray100),
                                borderRadius: BorderRadius.circular(6.0),
                              ),
                              hintText: '',
                            ),
                            child: DropdownButtonHideUnderline(
                              child: Obx(
                                () => DropdownButton<int>(
                                  value: controller.limitSeleted.value,
                                  icon: const Icon(Icons.arrow_drop_down),
                                  style: TextStyle(
                                    fontSize: 12.0,
                                    color: gray500,
                                  ),
                                  onChanged: (int? newValue) {
                                    if (newValue != null) {
                                      controller.limitSeleted.value = newValue;
                                    }
                                  },
                                  items: [25, 100, 250, 500]
                                      .map<DropdownMenuItem<int>>((int value) {
                                    return DropdownMenuItem<int>(
                                      value: value,
                                      child: Text(value.toString()),
                                    );
                                  }).toList(),
                                ),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 20),
                          child: SizedBox(
                            width: Get.mediaQuery.size.width,
                            height: 40,
                            child: MyButton(
                              text: 'Cari',
                              onPressed: () {
                                controller.offsetSeleted.value = 0;
                                controller.pageSelected.value = 1;
                                controller.cariPeserta();
                                Get.back();
                              },
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  backgroundColor: whiteColor,
                  isDismissible: true,
                  enableDrag: true,
                  shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20),
                      topRight: Radius.circular(20),
                    ),
                  ),
                );
              },
              icon: const Icon(Icons.filter_list),
            ),
          ],
        ),
        Obx(
          () => controller.isLoadingPeserta.value
              ? const Expanded(
                  child: ListLoading(),
                )
              : controller.pesertaList.isNotEmpty
                  ? Expanded(
                      child: ListPeserta(),
                    )
                  : const Expanded(
                      child: EmptyWidget(msg: 'Tidak terdapat data'),
                    ),
        ),
        const SizedBox(
          height: 15,
        ),
        Obx(
          () => !controller.isLoadingPeserta.value
              ? SizedBox(
                  height: 40,
                  child: ListView.builder(
                    itemCount: controller.totalPage.value + 1,
                    itemBuilder: (context, index) {
                      if (index > 0) {
                        return Obx(
                          () => SizedBox(
                            height: 40,
                            child: controller.pageSelected.value == index
                                ? ElevatedButton(
                                    style: ElevatedButton.styleFrom(
                                      primary: logoColor,
                                      elevation: 0,
                                      side: BorderSide(
                                          width: 1.0, color: logoColor),
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(0),
                                      ),
                                    ),
                                    onPressed: () {
                                      controller.offsetSeleted.value =
                                          controller.limitSeleted.value *
                                              (index - 1);
                                      controller.pageSelected.value = (index);
                                      controller.cariPeserta();
                                    },
                                    child: Text(index.toString()),
                                  )
                                : OutlinedButton(
                                    style: OutlinedButton.styleFrom(
                                      primary: logoColor,
                                      side: BorderSide(
                                          width: 1.0, color: logoColor),
                                      elevation: 0,
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(0),
                                      ),
                                    ),
                                    onPressed: () {
                                      controller.offsetSeleted.value =
                                          controller.limitSeleted.value *
                                              (index - 1);
                                      controller.pageSelected.value = index;
                                      controller.cariPeserta();
                                    },
                                    child: Text(index.toString()),
                                  ),
                          ),
                        );
                      } else {
                        return const SizedBox();
                      }
                    },
                    scrollDirection: Axis.horizontal,
                  ),
                )
              : Container(),
        ),
        const SizedBox(
          height: 15,
        ),
      ],
    );
  }
}

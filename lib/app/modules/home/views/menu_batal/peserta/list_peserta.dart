// ignore_for_file: must_be_immutable

import 'package:amanahgitha_flutter/app/data/models/peserta_menu_batal.dart';
import 'package:amanahgitha_flutter/app/modules/home/controllers/menu_batal/peserta_menu_batal_controller.dart';
import 'package:amanahgitha_flutter/app/routes/app_pages.dart';
import 'package:amanahgitha_flutter/app/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class ListPeserta extends StatelessWidget {
  ListPeserta({Key? key}) : super(key: key);

  final PesertaMenuBatalController controller = Get.find();

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      separatorBuilder: (context, index) => const SizedBox(
        height: 10,
      ),
      scrollDirection: Axis.vertical,
      itemCount: controller.pesertaList.length,
      itemBuilder: (BuildContext ctx, int index) {
        return InkWell(
          onTap: () {
            Get.toNamed(
              Routes.DETAIL_PESERTA_MENU_BATAL,
              arguments: controller.pesertaList[index].toJson(),
            );
          },
          child: SlideItem(item: controller.pesertaList[index]),
        );
      },
    );
  }
}

class SlideItem extends GetView<PesertaMenuBatalController> {
  const SlideItem({Key? key, required this.item}) : super(key: key);

  final Peserta item;

  @override
  Widget build(BuildContext context) {
    return Slidable(
      startActionPane: ActionPane(
        motion: const ScrollMotion(),
        children: [
          SlidableAction(
            onPressed: (ctx) {
              var params = {
                'pengguna': GetStorage().read('pengguna'),
                'policy_no': item.policyNo,
                'no_peserta': item.noPeserta,
              };
              controller.hapusPeserta(params);
            },
            backgroundColor: Colors.transparent,
            foregroundColor: dangerColor,
            icon: Icons.delete,
            label: 'Hapus',
          ),
        ],
      ),
      endActionPane: ActionPane(
        motion: const ScrollMotion(),
        children: [
          SlidableAction(
            onPressed: (ctx) {
              var params = {
                'pengguna': GetStorage().read('pengguna'),
                'policy_no': item.policyNo,
                'no_peserta': item.noPeserta,
              };
              controller.hapusPeserta(params);
            },
            backgroundColor: Colors.transparent,
            foregroundColor: dangerColor,
            icon: Icons.delete,
            label: 'Hapus',
          ),
        ],
      ),
      child: ItemPeserta(
        item: item,
      ),
    );
  }
}

class ItemPeserta extends StatelessWidget {
  ItemPeserta({Key? key, required this.item}) : super(key: key);

  final Peserta item;

  Color selectedColor = green400;
  Color unSelectedColor = gray;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.mediaQuery.size.width,
      height: 56.0,
      padding: const EdgeInsets.symmetric(horizontal: 14.0),
      decoration: BoxDecoration(
        color: backgroundColor,
        border: Border(
          left: BorderSide(
            color: unSelectedColor,
            width: 3.0,
          ),
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                item.noPeserta ?? '',
                style: TextStyle(color: gray, fontSize: 10),
              ),
              Text(
                item.nama ?? '',
                style: TextStyle(color: unSelectedColor, fontSize: 12),
              ),
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Text(
                "Tgl Lahir : ${item.tanggalLahir!.year.toString().padLeft(4, '0')}-${item.tanggalLahir!.month.toString().padLeft(2, '0')}-${item.tanggalLahir!.day.toString().padLeft(2, '0')}",
                style: TextStyle(color: gray, fontSize: 10),
              ),
              Text(
                "Awal Kontrak : ${item.fromDate!.year.toString().padLeft(4, '0')}-${item.fromDate!.month.toString().padLeft(2, '0')}-${item.fromDate!.day.toString().padLeft(2, '0')}",
                style: TextStyle(color: gray500, fontSize: 12),
              ),
              Text(
                "Akhir Kontrak : ${item.thruDate!.year.toString().padLeft(4, '0')}-${item.thruDate!.month.toString().padLeft(2, '0')}-${item.thruDate!.day.toString().padLeft(2, '0')}",
                style: TextStyle(color: gray500, fontSize: 12),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

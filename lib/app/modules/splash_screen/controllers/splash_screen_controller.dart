import 'package:amanahgitha_flutter/app/routes/app_pages.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class SplashScreenController extends GetxController {
  void checkForUpdate() {
    Future.delayed(const Duration(seconds: 2), () {
      if (GetStorage().read('pengguna') == null) {
        Get.offAllNamed(Routes.LOGIN);
      } else {
        Get.offAllNamed(Routes.HOME);
      }
    });
  }
}

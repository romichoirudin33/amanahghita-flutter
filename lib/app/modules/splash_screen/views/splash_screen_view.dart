// ignore_for_file: prefer_const_constructors, use_key_in_widget_constructors

import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/splash_screen_controller.dart';

class SplashScreenView extends GetView<SplashScreenController> {
  @override
  Widget build(BuildContext context) {
    controller.checkForUpdate();
    return Scaffold(
      body: Center(
        child: SizedBox(
          width: 150,
          child: Image.asset("assets/logo.png"),
        ),
      ),
    );
  }
}

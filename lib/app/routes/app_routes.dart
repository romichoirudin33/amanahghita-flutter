// ignore_for_file: constant_identifier_names

part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();
  static const HOME = _Paths.HOME;
  static const SPLASH_SCREEN = _Paths.SPLASH_SCREEN;
  static const LOGIN = _Paths.LOGIN;
  static const MENU_BATAL = _Paths.MENU_BATAL;
  static const DETAIL_PESERTA = _Paths.DETAIL_PESERTA;
  static const TAMBAH_ENTRY = _Paths.TAMBAH_ENTRY;
  static const DETAIL_PESERTA_BATAL = _Paths.DETAIL_PESERTA_BATAL;
  static const CABANG_SELECTED = _Paths.CABANG_SELECTED;
  static const DETAIL_PESERTA_MENU_BATAL = _Paths.DETAIL_PESERTA_MENU_BATAL;
}

abstract class _Paths {
  _Paths._();
  static const HOME = '/home';
  static const SPLASH_SCREEN = '/splash-screen';
  static const LOGIN = '/login';
  static const MENU_BATAL = '/menu-batal';
  static const DETAIL_PESERTA = '/detail-peserta';
  static const TAMBAH_ENTRY = '/tambah-entry';
  static const DETAIL_PESERTA_BATAL = '/detail-peserta-batal';
  static const CABANG_SELECTED = '/cabang-selected';
  static const DETAIL_PESERTA_MENU_BATAL = '/detail-peserta-menu-batal';
}

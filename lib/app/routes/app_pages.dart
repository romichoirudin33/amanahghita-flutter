import 'package:get/get.dart';

import '../modules/detail_peserta/bindings/detail_peserta_binding.dart';
import '../modules/detail_peserta/views/detail_peserta_view.dart';
import '../modules/detail_peserta_batal/bindings/detail_peserta_batal_binding.dart';
import '../modules/detail_peserta_batal/views/detail_peserta_batal_view.dart';
import '../modules/detail_peserta_menu_batal/bindings/detail_peserta_menu_batal_binding.dart';
import '../modules/detail_peserta_menu_batal/views/detail_peserta_menu_batal_view.dart';
import '../modules/home/bindings/home_binding.dart';
import '../modules/home/views/home_view.dart';
import '../modules/login/bindings/login_binding.dart';
import '../modules/login/views/login_view.dart';
import '../modules/splash_screen/bindings/splash_screen_binding.dart';
import '../modules/splash_screen/views/splash_screen_view.dart';
import '../modules/tambah_entry/bindings/tambah_entry_binding.dart';
import '../modules/tambah_entry/views/tambah_entry_view.dart';

// ignore_for_file: constant_identifier_names

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.SPLASH_SCREEN;

  static final routes = [
    GetPage(
      name: _Paths.HOME,
      page: () => HomeView(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.SPLASH_SCREEN,
      page: () => SplashScreenView(),
      binding: SplashScreenBinding(),
    ),
    GetPage(
      name: _Paths.LOGIN,
      page: () => LoginView(),
      binding: LoginBinding(),
    ),
    GetPage(
      name: _Paths.DETAIL_PESERTA,
      page: () => const DetailPesertaView(),
      binding: DetailPesertaBinding(),
      transition: Transition.rightToLeft,
      transitionDuration: const Duration(milliseconds: 500),
    ),
    GetPage(
      name: _Paths.TAMBAH_ENTRY,
      page: () => TambahEntryView(),
      binding: TambahEntryBinding(),
      transition: Transition.rightToLeft,
      transitionDuration: const Duration(milliseconds: 500),
    ),
    GetPage(
      name: _Paths.DETAIL_PESERTA_BATAL,
      page: () => const DetailPesertaBatalView(),
      binding: DetailPesertaBatalBinding(),
      transition: Transition.rightToLeft,
      transitionDuration: const Duration(milliseconds: 500),
    ),
    GetPage(
      name: _Paths.DETAIL_PESERTA_MENU_BATAL,
      page: () => const DetailPesertaMenuBatalView(),
      binding: DetailPesertaMenuBatalBinding(),
      transition: Transition.rightToLeft,
      transitionDuration: const Duration(milliseconds: 500),
    ),
  ];
}
